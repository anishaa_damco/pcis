package employeeIncident;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.JavascriptExecutor;
import org.testng.Assert;
import org.testng.annotations.Test;

import ClaimVisionPages.ClaimPage;
import ClaimVisionPages.ClaimantIncidentPage;
import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.EmployeeIncidentPage;
import ClaimVisionPages.LoginPage;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;
import CommonModules.Wait;

public class EmployeeIncident_Test {
	
	
		@Test(dataProvider = "EmployeIncident", dataProviderClass = ClaimVisionDataProvider.class)
		  public void employeeIncidentTest(HashMap<String, String> hm) {
			List<String> resultSetList = new ArrayList<String>();
			List<String> resultSetList1 = new ArrayList<String>();
			
			LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));		
			DashboardPage.getinstance().navigateToClaimSearch();
			/*if(hm.get("Scenario").equals("Status"))
				hm.put("ClaimNumber",BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%PND%' order by UpdateDate desc").get(0));
			else
			hm.put("ClaimNumber",BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%OPN%' order by UpdateDate desc").get(0));
			System.out.println(hm.get("ClaimNumber"));
			DashboardPage.getinstance().enterClaimNumber(hm.get("ClaimNumber"));
			DashboardPage.getinstance().clickSearch();
			DashboardPage.getinstance().selectFirstClaim(hm.get("ClaimNumber"));
			*/
			if(hm.get("Scenario").equals("Open")){
				resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%PND%' "
						+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')" 
						+ "order by UpdateDate desc");
				if(resultSetList.isEmpty())
				{
					BaseClass.logfail("No claim is present with pending status to verify the Open status scenario", "");
					//Assert.assertEquals(resultSetList.isEmpty(), false);
					
				}
				else{
					hm.put("ClaimNumber",resultSetList.get(0));
				}
			}
				
			else if (hm.get("Scenario").equals("ReOpened"))
			{
				resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where (ClaimStatusCode like '%RCL%' "
						+ "or ClaimStatusCode like '%CLS%') "
						+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
						+ "order by UpdateDate desc");
				if(resultSetList.isEmpty())
				{
					resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%OPN%' "
							+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
						+ "order by UpdateDate desc");
					if(resultSetList.isEmpty())
					{
						resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%PND%' "
								+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
								+ "order by UpdateDate desc");
						if(resultSetList.isEmpty())
						{
							BaseClass.logfail("No claim is present with pending or open or closed or reclosed status to verify the Reopened status scenario", "");
							//Assert.assertEquals(resultSetList.isEmpty(), false);
							
						}
						else{
							hm.put("ClaimNumber",resultSetList.get(0));
						}
					}
					else{
						//hm.put("ClaimNumber",resultSetList.get(0));
						BaseClass.logInfo("Claim with Open status is picked", "");
						resultSetList1 = BaseClass.getqueryresult("select PaymentStatusId from ClaimPaymentDetail where ClaimPaymentId in("
								+"select  ClaimPaymentId from ClaimPayment where ClaimNumber like '%"
								+ resultSetList.get(0)+"%')");
						hm.put("ClaimNumber",resultSetList.get(0));
						if(resultSetList1.isEmpty()){
							hm.put("PaymentStatus","0");
								
						}
						else{
							hm.put("PaymentStatus",resultSetList1.get(0));
						}
					}
					
				}
				else{
					hm.put("ClaimNumber",resultSetList.get(0));
				}
				
				
			}
			else if (hm.get("Scenario").equals("Closed"))
				{
				resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%OPN%' "
						+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
						+ "order by UpdateDate desc");
				
				if(resultSetList.isEmpty())
				{
					resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%PND%' "
							+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
							+ "order by UpdateDate desc");
					if(resultSetList.isEmpty())
					{
						BaseClass.logfail("No claim is present with pending or open or closed or reclosed status to verify the Reopened status scenario", "");
						//Assert.assertEquals(resultSetList.isEmpty(), false);
						
					}
					else{
						hm.put("ClaimNumber",resultSetList.get(0));
					}
				}
					else{

						BaseClass.logInfo("Claim with Open status is picked", "");
						resultSetList1 = BaseClass.getqueryresult("select PaymentStatusId from ClaimPaymentDetail where ClaimPaymentId in("
								+"select  ClaimPaymentId from ClaimPayment where ClaimNumber like '%"
								+ resultSetList.get(0)+"%')");
						hm.put("ClaimNumber",resultSetList.get(0));
						if(resultSetList1.isEmpty()){
							hm.put("PaymentStatus","0");
								
						}
						else{
							hm.put("PaymentStatus",resultSetList1.get(0));
						}
						
					}
				}
			
			else if (hm.get("Scenario").equals("ReClosed"))
			{
				resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%ROP%' "
					+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
						+ " order by UpdateDate desc");
				if(resultSetList.isEmpty())
				{
				resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%CLS%' "
						+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
						+ " order by UpdateDate desc");
				
				if(resultSetList.isEmpty())
				{
					resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%OPN%' "
							+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
							+ "order by UpdateDate desc");
					if(resultSetList.isEmpty())
					{
						resultSetList = BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%PND%' "
								+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
								+ "order by UpdateDate desc");
						if(resultSetList.isEmpty())
						{
							BaseClass.logfail("No claim is present with pending or open or closed or reopened status to verify the ReClosed status scenario", "");
							//Assert.assertEquals(resultSetList.isEmpty(), false);
							
						}
						else{
							BaseClass.logInfo("Claim with pending claim is picked", "");
							hm.put("ClaimNumber",resultSetList.get(0));
						}
					}// pending claim if loop is finished
					else{
						BaseClass.logInfo("Claim with Open status is picked", "");
						resultSetList1 = BaseClass.getqueryresult("select PaymentStatusId from ClaimPaymentDetail where ClaimPaymentId in("
								+"select  ClaimPaymentId from ClaimPayment where ClaimNumber like '%"
								+ resultSetList.get(0)+"%')");
						hm.put("ClaimNumber",resultSetList.get(0));
						if(resultSetList1.isEmpty()){
							hm.put("PaymentStatus","0");
								
						}
						else{
							hm.put("PaymentStatus",resultSetList1.get(0));
						}
					}
					
				}//open claim if loop is finished
				else{
					BaseClass.logInfo("Claim with closed status is picked", "");
					hm.put("ClaimNumber",resultSetList.get(0));
					
				}
			}// closed claim if loop is finished
				else{
					BaseClass.logInfo("Claim with Re-Opened status is picked", "");
					resultSetList1 = BaseClass.getqueryresult("select PaymentStatusId from ClaimPaymentDetail where ClaimPaymentId in("
							+"select  ClaimPaymentId from ClaimPayment where ClaimNumber like '%"
							+ resultSetList.get(0)+"%')");
					hm.put("ClaimNumber",resultSetList.get(0));
					if(resultSetList1.isEmpty()){
						hm.put("PaymentStatus","0");
							
					}
					else{
						hm.put("PaymentStatus",resultSetList1.get(0));
					}
				}
			}// re-opened claim if loop is finished
			//reclosed scenario is finished
				
			else
			hm.put("ClaimNumber",BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%OPN%' "
					+"and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation')"
					+"order by UpdateDate desc").get(0));
			System.out.println(hm.get("ClaimNumber"));
			
			System.out.println(hm.get("Scenario"));
			if(hm.get("ClaimNumber")!=null){
				DashboardPage.getinstance().enterClaimNumber(hm.get("ClaimNumber"));
				DashboardPage.getinstance().clickSearch();
				DashboardPage.getinstance().selectFirstClaim(hm.get("ClaimNumber"));
				BaseClass.logInfo("Selected Claim number for testing:", hm.get("ClaimNumber"));
				DashboardPage.getinstance().navigateToEmployeeIncident(hm);
				Wait.waitForTitle(BaseClass.getDriver(), "Employee/Incident");
				BaseClass.logInfo("Navigate to Employee/Incident Screen", "");
				
				if (hm.get("Scenario").equals("CancelButton"))
					EmployeeIncidentPage.getinstance().cancelButtonVerify();
				else if (hm.get("Scenario").equals("InvalidData"))
					EmployeeIncidentPage.getinstance().invalidDataValidation();
				
				else if (hm.get("Scenario").equals("ReClosed") || hm.get("Scenario").equals("Closed")
						|| hm.get("Scenario").equals("Open") || hm.get("Scenario").equals("ReOpened"))
				{
					System.out.println("Status test case");
					System.out.println(resultSetList.get(0));
					EmployeeIncidentPage.getinstance().claimStatusVerification(hm);
					}
				else if (hm.get("Scenario").equals("updateDetails")){
					EmployeeIncidentPage.getinstance().updateEmployeeIncidentDetails();
				}
			}
				
			
		}
		
	}



