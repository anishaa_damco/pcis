package Claim;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.Test;

import com.aventstack.extentreports.Status;

import ClaimVisionPages.ClaimPage;
import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.EmployeeIncidentPage;
import ClaimVisionPages.LoginPage;
import ClaimVisionPages.PaymentPage;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;
import CommonModules.Wait;

public class Claim_Test {
	
	@Test(dataProvider = "Claim", dataProviderClass = ClaimVisionDataProvider.class)
	  public void claimTest(HashMap<String, String> hm) {

		List<String> resultSetList = new ArrayList<String>();
		List<String> resultSetList1 = new ArrayList<String>();
		LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		
		DashboardPage.getinstance().navigateToClaimSearch();
		if(hm.get("Scenario").equals("Open")){
			resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
					+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
					+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%PND%'" 
					+" order by c.UpdateDate desc");
			if(resultSetList.isEmpty())
			{
				BaseClass.logfail("No claim is present with pending status to verify the Open status scenario", "");
			}
			else{
				hm.put("ClaimNumber",resultSetList.get(0));
			}
		}
			
		else if (hm.get("Scenario").equals("ReOpened"))
		{
			resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c join PolicyType pt" 
					+" on c.policytypeid  = pt.policytypeid where PolicyTypeDescription not like '%Compensation'"
					+" and (ClaimStatusCode like '%RCL%' or ClaimStatusCode like '%CLS%')" 
					+ " order by UpdateDate desc");
			if(resultSetList.isEmpty())
			{
				resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
						+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
						+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%OPN%'" 
						+" order by c.UpdateDate desc");
				if(resultSetList.isEmpty())
				{
					resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
					+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
					+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%PND%'" 
					+" order by c.UpdateDate desc");
					if(resultSetList.isEmpty())
					{
						BaseClass.logfail("No claim is present with pending or open or closed or reclosed status to verify the Reopened status scenario", "");
					}
					else{
						hm.put("ClaimNumber",resultSetList.get(0));
					}
				}
				else{
					//hm.put("ClaimNumber",resultSetList.get(0));
					BaseClass.logInfo("Claim with Open status is picked", "");
					resultSetList1 = BaseClass.getqueryresult("select PaymentStatusId from ClaimPaymentDetail where ClaimPaymentId in("
							+"select  ClaimPaymentId from ClaimPayment where ClaimNumber like '%"
							+ resultSetList.get(0)+"%')");
					hm.put("ClaimNumber",resultSetList.get(0));
					if(resultSetList1.isEmpty()){
						hm.put("PaymentStatus","0");
							
					}
					else{
						hm.put("PaymentStatus",resultSetList1.get(0));
					}
				}
				
			}
			else{
				hm.put("ClaimNumber",resultSetList.get(0));
			}
			
			
		}
		else if (hm.get("Scenario").equals("Closed"))
			{
			resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
					+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
					+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%OPN%'" 
					+" order by c.UpdateDate desc");
			
			if(resultSetList.isEmpty())
			{
				resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
						+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
						+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%PND%'" 
						+" order by c.UpdateDate desc");
				if(resultSetList.isEmpty())
				{
					BaseClass.logfail("No claim is present with pending or open or closed or reclosed status to verify the Reopened status scenario", "");
				}
				else{
					hm.put("ClaimNumber",resultSetList.get(0));
				}
			}
			else{

				BaseClass.logInfo("Claim with Open status is picked", "");
				resultSetList1 = BaseClass.getqueryresult("select PaymentStatusId from ClaimPaymentDetail where ClaimPaymentId in("
						+"select  ClaimPaymentId from ClaimPayment where ClaimNumber like '%"
						+ resultSetList.get(0)+"%')");
				hm.put("ClaimNumber",resultSetList.get(0));
				if(resultSetList1.isEmpty()){
					hm.put("PaymentStatus","0");
						
				}
				else{
					hm.put("PaymentStatus",resultSetList1.get(0));
				}
				
			}

			}
		
		else if (hm.get("Scenario").equals("ReClosed"))
		{
			resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
					+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
					+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%ROP%'" 
					+" order by c.UpdateDate desc");
			if(resultSetList.isEmpty())
			{
			resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
					+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
					+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%CLS%'" 
					+" order by c.UpdateDate desc");
			
			if(resultSetList.isEmpty())
			{
				resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
						+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
						+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%OPN%'" 
						+" order by c.UpdateDate desc");
				if(resultSetList.isEmpty())
				{
					resultSetList = BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
							+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
							+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%PND%'" 
							+" order by c.UpdateDate desc");
					if(resultSetList.isEmpty())
					{
						BaseClass.logfail("No claim is present with pending or open or closed or reopened status to verify the ReClosed status scenario", "");
					}
					else{
						BaseClass.logInfo("Claim with pending claim is picked", "");
						hm.put("ClaimNumber",resultSetList.get(0));
					}
				}// pending claim if loop is finished
				else{
					BaseClass.logInfo("Claim with Open status is picked", "");
					resultSetList1 = BaseClass.getqueryresult("select PaymentStatusId from ClaimPaymentDetail where ClaimPaymentId in("
							+"select  ClaimPaymentId from ClaimPayment where ClaimNumber like '%"
							+ resultSetList.get(0)+"%')");
					hm.put("ClaimNumber",resultSetList.get(0));
					if(resultSetList1.isEmpty()){
						hm.put("PaymentStatus","0");
							
					}
					else{
						hm.put("PaymentStatus",resultSetList1.get(0));
					}
				}
				
			}//open claim if loop is finished
			else{
				BaseClass.logInfo("Claim with closed status is picked", "");
				hm.put("ClaimNumber",resultSetList.get(0));
				
			}
		}// closed claim if loop is finished
			else{
				BaseClass.logInfo("Claim with Re-Opened status is picked", "");
				resultSetList1 = BaseClass.getqueryresult("select PaymentStatusId from ClaimPaymentDetail where ClaimPaymentId in("
						+"select  ClaimPaymentId from ClaimPayment where ClaimNumber like '%"
						+ resultSetList.get(0)+"%')");
				hm.put("ClaimNumber",resultSetList.get(0));
				if(resultSetList1.isEmpty()){
					hm.put("PaymentStatus","0");
						
				}
				else{
					hm.put("PaymentStatus",resultSetList1.get(0));
				}
			}
		}// re-opened claim if loop is finished
		//reclosed scenario is finished
			
		else
		hm.put("ClaimNumber",BaseClass.getqueryresult("select  top 1 c.ClaimNumber from claim c" 
				+" join PolicyType pt on c.policytypeid  = pt.policytypeid"
				+" where PolicyTypeDescription not like '%Compensation' and ClaimStatusCode like '%OPN%'" 
				+" order by c.UpdateDate desc").get(0));

		/*if(hm.get("Scenario").equals("Open"))
			hm.put("ClaimNumber",BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%PND%' order by UpdateDate desc").get(0));
		else if (hm.get("Scenario").equals("Closed"))
			hm.put("ClaimNumber",BaseClass.getqueryresult("Select top 1 cl.claimnumber from claim cl"+
						" inner join ClaimPayment ps on cl.claimnumber = ps.claimnumber"+
						" where ps.PaymentStatusId <> 1 and cl.ClaimStatusCode like '%OPN%'"+
						" order by cl.UpdateDate desc").get(0));
		else if (hm.get("Scenario").equals("ReClosed"))
				hm.put("ClaimNumber",BaseClass.getqueryresult("Select top 1 cl.claimnumber from claim cl"+
						" inner join ClaimPayment ps on cl.claimnumber = ps.claimnumber"+
						" where ps.PaymentStatusId <> 1 and cl.ClaimStatusCode like '%ROP%' order by cl.UpdateDate desc").get(0));
		else if (hm.get("Scenario").equals("ReOpened"))
			hm.put("ClaimNumber",BaseClass.getqueryresult("select top 1 ClaimNumber from claim where ClaimStatusCode like '%RCL%' or ClaimStatusCode like '%CLS%' order by UpdateDate desc").get(0));
		*/

		System.out.println(hm.get("ClaimNumber"));
		
		System.out.println(hm.get("Scenario"));
		if(hm.get("ClaimNumber")!=null){
			DashboardPage.getinstance().enterClaimNumber(hm.get("ClaimNumber"));
			DashboardPage.getinstance().clickSearch();
			DashboardPage.getinstance().selectFirstClaim(hm.get("ClaimNumber"));
			BaseClass.logInfo("Selected Claim number for testing:", hm.get("ClaimNumber"));
			Wait.waitForTitle(BaseClass.getDriver(), "Claim");
			BaseClass.logInfo("Navigate to Claim Screen", "");
			if (hm.get("Scenario").equals("ReClosed") || hm.get("Scenario").equals("Closed")
					|| hm.get("Scenario").equals("Open") || hm.get("Scenario").equals("ReOpened"))
					ClaimPage.getinstance().claimStatusVerification(hm);
			else if (hm.get("Scenario").equals("Denied"))
				ClaimPage.getinstance().claimDenied();
			else if (hm.get("Scenario").equals("Include All Payment Categories"))
				ClaimPage.getinstance().includeAllPayCatInAggregateLimits();
			else if ((hm.get("Scenario").equals("Incident Description"))
					|| (hm.get("Scenario").equals("Claim Summary"))
					|| (hm.get("Scenario").equals("Claim Damages")))
				ClaimPage.getinstance().limitVerification(hm.get("Scenario"));
			else if (hm.get("Scenario").equals("CancelButton"))
				ClaimPage.getinstance().cancelButtonVerify();
		}
	  }
	
	/*@Test(dataProvider = "Claim", dataProviderClass = ClaimVisionDataProvider.class)
	public void suspendedClaimCheckboxVerify() throws Exception
	{
		ClaimPage wc_claimant = null;
		wc_claimant.selectClaimSuspended();
		Wait.waitFor(2);
		wc_claimant.selectSuspensionLevel();
		wc_claimant.selectSuspensionType();
		wc_claimant.enterSuspensionEffDate();
		wc_claimant.enterSuspensionNarrative();
		Wait.waitFor(2);
		wc_claimant.clickOnSaveButton();
		
		boolean status = wc_claimant.suspendedCheckboxStatus();
		Assert.assertEquals(status, true);
		
	}*/

}
