package Claim;

import java.util.HashMap;

import org.testng.annotations.Test;

import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.LoginPage;
import ClaimVisionPages.ReservePage;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;

public class FinancialSummary {
	@Test(dataProvider = "FinancialSummary", dataProviderClass = ClaimVisionDataProvider.class)
	public void Reserve(HashMap<String, String> hm) {
		LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		DashboardPage.getinstance().navigateToClaimSearch();
		if(hm.get("PolicyType").contains("Compensation"))
		{
		hm.put("ClaimNumber",BaseClass.getqueryresult("Select top 1 ClaimNumber from claim where ClaimStatusCode in ('OPN','ROP')	"
				+ "and PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription like '%Compensation') order by claimid desc").get(0));
		}
		else
		{
			hm.put("ClaimNumber",BaseClass.getqueryresult("Select TOP 1 a.ClaimNumber,b.ClaimantId from claim as a "
+"join claimant as b	 ON a.ClaimId=b.ClaimId "
+"join ClaimantSubClaim as c ON b.ClaimantId=c.ClaimantId "
+"where a.ClaimStatusCode in ('OPN','ROP') "
+"and a.PolicyTypeId=(Select policytypeid from PolicyType where PolicyTypeDescription='"+hm.get("PolicyType")+"') "
+"and c.ClaimantId not in (Select claimantid from ClaimantSubClaim where SubClaimNumber=2 OR ClaimStatusCode not in ('OPN','ROP')) "
+"order by a.EntryDate desc").get(0));
		}
		DashboardPage.getinstance().enterClaimNumber(hm.get("ClaimNumber"));
		DashboardPage.getinstance().clickSearch();
		DashboardPage.getinstance().selectFirstClaim(hm.get("ClaimNumber"));
		if(!hm.get("CustomerCode").contains("NYLAW"))
		{
		DashboardPage.getinstance().navigateToReserve();
		ReservePage.getinstance().setReserve(hm);
		}
	}
}
