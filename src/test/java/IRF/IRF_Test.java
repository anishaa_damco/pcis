package IRF;

import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.IRFPage;
import ClaimVisionPages.LoginPage;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;

public class IRF_Test {

	@Test(dataProvider = "IRF", dataProviderClass = ClaimVisionDataProvider.class)
  public void createIRF(HashMap<String, String> hm) {
		

	  LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
	  String parent=BaseClass.getcurrentwindow();
	  DashboardPage.getinstance().navigateToIRF(parent);
	  IRFPage.getinstance().createIRF(hm);
	  BaseClass.closeWindow();
	  BaseClass.switchToWindow(parent, 1);	  
  }
}
