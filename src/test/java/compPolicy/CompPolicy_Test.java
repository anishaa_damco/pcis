/**
 * 
 */
package compPolicy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.testng.annotations.Test;

import ClaimVisionPages.DashboardPage;
import ClaimVisionPages.LoginPage;
import ClaimVisionPages.MedicalTreatmentPage;
import ClaimVisionPages.compPolicySetupPage;
import CommonModules.BaseClass;
import CommonModules.ClaimVisionDataProvider;
import CommonModules.Wait;

/**
 * @author aagrawal
 *
 */
public class CompPolicy_Test {
	
	@Test(dataProvider = "CompPolicyData", dataProviderClass = ClaimVisionDataProvider.class)
	public void compPolicy(HashMap <String, String> hm){
		//List<String> resultSet1 = new ArrayList<String> ();
		//List<String> resultSet2 = new ArrayList<String> ();
		LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));		
		DashboardPage.getinstance().navigateToCompPolicy();
		Wait.waitForTitle(BaseClass.getDriver(), "Policy Set-up");
		BaseClass.logInfo("Navigate to Policy Set-up Screen", "");
		if (hm.get("Scenario").equals("Add Policy")){
			compPolicySetupPage.getinstance().AddPolicy(hm);
			System.out.println(BaseClass.getTitle());
		}
		
		
	}

}
