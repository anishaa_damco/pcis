package Vendor;

import java.util.*;

import org.testng.annotations.Test;

import CommonModules.*;
import ClaimVisionPages.*;



public class VendorMergeTest {

	
	@Test(dataProvider = "VendorMerge", dataProviderClass = ClaimVisionDataProvider.class)
	  public void createIRF(HashMap<String, String> hm) {

		  LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		  DashboardPage.getinstance().navigateToVendorMerge();
		  if(hm.get("Scenario").equalsIgnoreCase("Search"))
		  {
			  switch(hm.get("SearchField"))
			  {
			  case "First Name":
			  {
				  VendorMergePage.getinstance().searchByFirstName();
				  break;
			  }
			  case "Middle Name":
			  {
				  VendorMergePage.getinstance().searchByMiddleName();
				  break;
			  }
			  case "Last Name":
			  {
				  VendorMergePage.getinstance().searchByLastName();
				  break;
			  }
			  case "Vendor Name":
			  {
				  VendorMergePage.getinstance().searchByVendorName();
				  break;
			  }
			  case "VendorID":
			  {
				  VendorMergePage.getinstance().searchByVendorID();
				  break;
			  }
			  case "Alt VendorID":
			  {
				  VendorMergePage.getinstance().searchByAltVendorID();
				  break;
			  }
			  case "FEIN":
			  {
				  VendorMergePage.getinstance().searchByFEIN();
				  break;
			  }
			  case "Address":
			  {
				  VendorMergePage.getinstance().searchByAddress();
				  break;
			  }
			  case "ZIP":
			  {
				  VendorMergePage.getinstance().searchByZIP();
				  break;
			  }
			  default:
				  BaseClass.logfail("", "Wrong Data in Excel File");
			  }
		  }
		  else
		  {
			  VendorSetUpPage.getinstance().VendorSetUp_CreateVendor(hm);
		  }
	  }

}
