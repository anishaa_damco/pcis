package Vendor;

import java.util.*;

import org.testng.annotations.Test;

import CommonModules.*;
import ClaimVisionPages.*;



public class VendorSetUpTest {

	
	@Test(dataProvider = "Vendor", dataProviderClass = ClaimVisionDataProvider.class)
	  public void createIRF(HashMap<String, String> hm) {

		  LoginPage.getinstance().login(hm.get("CustomerCode"),hm.get("UserName"), hm.get("Password"));
		  DashboardPage.getinstance().navigateToVendorSetup();
		  if(hm.get("Scenario").equalsIgnoreCase("Mandatory"))
		  {
			  VendorSetUpPage.getinstance().VendorSetUp_MandatoryCheck();
		  }
		  else
		  {
			  VendorSetUpPage.getinstance().VendorSetUp_CreateVendor(hm);
		  }
	  }

}
