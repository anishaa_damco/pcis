package CommonModules;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class Wait {

	public static WebDriverWait webDriverWait;
	
	public static void waitforframe(WebDriver driver, WebElement element)
	{
		WebDriverWait wait = new WebDriverWait(driver,10);
		
		
		  wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(element));
	        
	}
	

	
	//This method is used to wait till element is clickable
	public static void modifyWait( WebDriver driver, WebElement element)
	{
		int i=0;
		boolean temp =false;
		while(temp ==false)
		{	
			try{
				if(i>=10)
				{
					temp=true;
					
				}
				i++;
				webDriverWait = new WebDriverWait(driver, 10);

				webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
				temp=true;
				
			}
			
			catch(Exception e)
			{
				
				temp=false;
				waitFor(10);
			}
			
		}
	}
	public static void waitFortextPresent(WebDriver driver,final WebElement element)
	{
		
		
		webDriverWait.until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return element.getText().length() != 0;
            }
        });
		
	}
	
	public static void waitForValuePresent(WebDriver driver,final WebElement element)
	{
		(new WebDriverWait(driver, 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
            	  return element.getAttribute("value").length() != 0; 
            }
        });
	}
	//This method is used to wait till the element is visible
	public static void waitTillPresent( WebDriver driver, WebElement element) 
	{
				webDriverWait = new WebDriverWait(driver, 20);

				webDriverWait.until(ExpectedConditions.visibilityOf(element));
	}
		
	public static void waitFor(long time)
	{
		try {
				Thread.sleep(time * 1000);
			}
		catch (Exception e) 
			{
				System.out.println("Exception in thread sleep");
			}
	}
	
	public static void implicitWait(WebDriver driver, int wait) 
	{
		driver.manage().timeouts().implicitlyWait(wait, TimeUnit.SECONDS);
	}
	public static void waitforchildwindow(int count)
	{
		(new WebDriverWait(BaseClass.getDriver(), 20)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return d.getWindowHandles().size() == count;
            }
        });
	}
	
	public static void waitForTitle( WebDriver driver, String title) 
	{
		webDriverWait = new WebDriverWait(driver, 20);

		webDriverWait.until(ExpectedConditions.titleIs(title));		
	}
	
	public static void waitForIsSelected(WebDriver driver, WebElement e){
		webDriverWait = new WebDriverWait(driver, 20);

		webDriverWait.until(ExpectedConditions.elementToBeSelected(e));
	}
	
	public static void waitForAlert( WebDriver driver) 
	{
		webDriverWait = new WebDriverWait(driver, 20);

		webDriverWait.until(ExpectedConditions.alertIsPresent());			
	}
	public static void waitForEnable(WebDriver driver, WebElement element) {
		webDriverWait = new WebDriverWait(driver, 20);

		webDriverWait.until(ExpectedConditions.elementToBeClickable(element));
		
	}
	public static void waitforOptions(final WebElement element) {
		// TODO Auto-generated method stub
		(new WebDriverWait(BaseClass.getDriver(), 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
            	  return new Select(element).getOptions().size() >1; 
            }
        });
		
	}



	public static void waitforelement(WebDriver driver,WebElement locator) {
		
	        	webDriverWait = new WebDriverWait(driver, 20);
		webDriverWait.until(ExpectedConditions.visibilityOf(locator));
		}
	       
		
	
	
	
	public static void waitforelements(WebDriver driver,List<WebElement> locator) {
		

		webDriverWait.until(ExpectedConditions.visibilityOfAllElements(locator));}
	       
	
	public static boolean isloadComplete(WebDriver driver)
	{
	    return ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("loaded")
	            || ((JavascriptExecutor) driver).executeScript("return document.readyState").equals("complete");
	}
	

	public static void waitForElementdisappear(WebElement element) {
		new WebDriverWait(BaseClass.getDriver(), 20).until(ExpectedConditions.invisibilityOf(element));
	}


	public static void waitForContainValue(WebElement element, String value) {
		(new WebDriverWait(BaseClass.getDriver(), 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
            	  return element.getAttribute("value").contains(value);
            }
        });
		
	}



	public static void waitForNotContainValue(WebElement element, String value) {
		/*(new WebDriverWait(BaseClass.getDriver(), 10)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
            	  return (!element.getAttribute("value").contains(value));
            }
        });*/
		BaseClass.waitForNotContainValue(element,value);
		
	}



	public static void waitforTextDisappear(WebElement element) {
		(new WebDriverWait(BaseClass.getDriver(), 30)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return element.getText().length() == 0;
            }
        });
		
	}



	public static void waitForTextChanged(WebElement element, String value) {
		(new WebDriverWait(BaseClass.getDriver(), 20)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return !element.getText().equals(value);
            }
        });
		
	}
	
	public static void waitForTextUpdate(WebElement element, String ExpectedValue) {
		(new WebDriverWait(BaseClass.getDriver(), 20)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return element.getText().contains(ExpectedValue);
            }
        });
		
	}
	
	public static void waitForElementStale(WebDriver driver,WebElement element) {
		webDriverWait = new WebDriverWait(driver, 20);


		webDriverWait.until(ExpectedConditions.stalenessOf(element));

		
	}



	public static void waitForDisable(WebElement element) {
		(new WebDriverWait(BaseClass.getDriver(), 20)).until(new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver d) {
                return !element.isEnabled();
            }
        });
		
	}
		
}

