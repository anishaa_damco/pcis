package CommonModules;

import java.io.File;
import java.util.Calendar;
import java.util.Date;

import com.relevantcodes.extentreports.DisplayOrder;
import com.relevantcodes.extentreports.ExtentReports;
import com.relevantcodes.extentreports.NetworkMode;


public class ReportManager {
	
	
	 private static ExtentReports extent;
	 //This method is used to create the object of extent report  
	 
	


	  public static String generateReport() {
		   
		   Calendar cal = Calendar.getInstance();   
		   Date time=cal.getTime();
		   String timestamp=time.toString();
		   String systime=timestamp.replace(":", "-");
           String reportFolder=System.getProperty("user.dir")+"//Report//Report_"+BaseClass.getCurrentDatetimeNow()+"//";
		   String fileName= reportFolder + systime +".html";
		   TakeScreenshot.setscreenshotpath(reportFolder);
		 extent = new ExtentReports(fileName);
	    return fileName;
	   }
	 public synchronized static ExtentReports getReporter(String customerCode) {
	        if (extent == null) {
	               extent = new ExtentReports(generateReport(),false, DisplayOrder.OLDEST_FIRST, NetworkMode.ONLINE);
	            extent
	                .addSystemInfo("Host Name", "PCIS")
	                .addSystemInfo("Executed By", "Anisha Agrawal")
	                .addSystemInfo("Environment", customerCode);
	            extent.loadConfig(new File(""));
	            	
	        }
	        
	        return extent;
	    }
	 
	
}