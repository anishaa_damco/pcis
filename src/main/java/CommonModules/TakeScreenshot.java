package CommonModules;

import java.io.File;
import java.io.IOException;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;

public class TakeScreenshot {
	
	static int counter=1;
	static String filelocation="";
	
	public static void setscreenshotpath(String path)
	{
		filelocation=path+"Screenshots//";
	}
	public static File getscreenshot(WebDriver driver)
	{ 
		TakesScreenshot scrShot =((TakesScreenshot)driver);

        //Call getScreenshotAs method to create image file

                File SrcFile=scrShot.getScreenshotAs(OutputType.FILE);

            //Move image file to new destination

                File DestFile=new File(filelocation+"image"+counter+".jpg");
                counter++;

                //Copy file at destination

                try {
					FileUtils.copyFile(SrcFile, DestFile);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					System.out.println("Screenshot not captured");
					return new File(System.getProperty("user.dir")+"//Sorry-Image.jpg");
				}
		return DestFile;
	}

}
