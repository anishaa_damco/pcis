package ClaimVisionPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class VendorMergePage {
	
	private VendorMergePage(){}
	private static final VendorMergePage vendorMerge=new VendorMergePage();

	public static VendorMergePage getinstance()
	{
		return vendorMerge;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="VendorsMerge_zipTextBox")
	WebElement vendorMergeZip;
	@FindBy(id="VendorsMerge_searchButton")
	WebElement vendorMergeSearch;
	@FindBy(id="VendorsMerge_clearAllButton")
	WebElement vendorMergeClearAll;
	@FindBy(id="VendorsMerge_firstNameTextBox")
	WebElement vendorMergeFirstName;
	@FindBy(id="VendorsMerge_middleNameTextBox")
	WebElement vendorMergeMiddleName;
	@FindBy(id="VendorsMerge_lastNameTextBox")
	WebElement vendorMergeLastName;
	@FindBy(id="VendorsMerge_companyNameTextBox")
	WebElement vendorMergeVendorName;
	@FindBy(id="VendorsMerge_billerTextBox")
	WebElement vendorMergeVendorId;
	@FindBy(id="VendorsMerge_altVendorNoTextBox")
	WebElement vendorMergeAltVendorId;
	@FindBy(id="VendorsMerge_feinTextBox")
	WebElement vendorMergeFIEN;
	@FindBy(id="VendorsMerge_addressTextBox")
	WebElement vendorMergeAddress;
	@FindBy(id="VendorsMerge_CityDropDownList")
	WebElement vendorMergeCity;
	@FindBy(id="VendorsMerge_stateDropDown")
	WebElement vendorMergeState;
	@FindBy(id="VendorsMerge_VendorPager_lblResults")
	WebElement VendorMergeTotalRecord;
	
	
	String searchableField;
	public void searchByFirstName()
	{
		searchableField=BaseClass.getqueryresult("Select TOP 1 ContactFirstName from vendor order by VendorId desc").get(0);
		BaseClass.settext(vendorMergeFirstName, searchableField);
		BaseClass.logInfo("First Name is set on Vendor Merge Screen as:", BaseClass.getattribute(vendorMergeFirstName, "value"));
		clickSearch();
		verifyTotalRecords(BaseClass.getqueryresult("Select count(*) from vendor where ContactFirstName like '%"+searchableField+"%'").get(0));
		}
	public void searchByMiddleName()
	{
		searchableField=BaseClass.getqueryresult("Select TOP 1 ContactMiddleName from vendor order by VendorId desc").get(0);
		BaseClass.settext(vendorMergeMiddleName, searchableField);
		BaseClass.logInfo("Middle Name is set on Vendor Merge Screen as:", BaseClass.getattribute(vendorMergeMiddleName, "value"));
		clickSearch();
		verifyTotalRecords(BaseClass.getqueryresult("Select count(*) from vendor where ContactMiddleName like '%"+searchableField+"%'").get(0));
	}
	
	public void searchByLastName()
	{
		searchableField=BaseClass.getqueryresult("Select TOP 1 ContactLastName from vendor order by VendorId desc").get(0);
		BaseClass.settext(vendorMergeLastName, searchableField);
		BaseClass.logInfo("Last Name is set on Vendor Merge Screen as:", BaseClass.getattribute(vendorMergeLastName, "value"));
		clickSearch();
		verifyTotalRecords(BaseClass.getqueryresult("Select count(*) from vendor where ContactLastName like '%"+searchableField+"%'").get(0));
	}
	
	public void searchByVendorName()
	{
		searchableField=BaseClass.getqueryresult("Select TOP 1 VendorName from vendor order by VendorId desc").get(0);
		BaseClass.settext(vendorMergeVendorName, searchableField);
		BaseClass.logInfo("Vendor Name is set on Vendor Merge Screen as:", BaseClass.getattribute(vendorMergeVendorName, "value"));
		clickSearch();
		verifyTotalRecords(BaseClass.getqueryresult("Select count(*) from vendor where VendorName like '%"+searchableField+"%'").get(0));
	}
	
	private void verifyTotalRecords(String total)
	{
		Assert.assertTrue(BaseClass.gettext(VendorMergeTotalRecord).endsWith("of "+total));	
		BaseClass.logpass("Total Records should be "+total, "Total Records are "+total);
	}
	
	private void clickSearch()
	{
		BaseClass.click(vendorMergeSearch);
		BaseClass.logInfo("Search Button is Clicked", "");
	    Wait.waitforelement(BaseClass.getDriver(), VendorMergeTotalRecord);
	}
	public void searchByVendorID() {
		searchableField=BaseClass.getqueryresult("Select TOP 1 VendorID from vendor order by VendorId desc").get(0);
		BaseClass.settext(vendorMergeVendorId, searchableField);
		BaseClass.logInfo("Vendor# is set on Vendor Merge Screen as:", BaseClass.getattribute(vendorMergeVendorId, "value"));
		clickSearch();
		verifyTotalRecords(BaseClass.getqueryresult("Select count(*) from vendor where VendorID like '%"+searchableField+"%'").get(0));
	}
	public void searchByAltVendorID() {
		searchableField=BaseClass.getqueryresult("Select TOP 1 VendorNumber from vendor where VendorNumber is not null").get(0);
		BaseClass.settext(vendorMergeAltVendorId, searchableField);
		BaseClass.logInfo("Alt Vendor# is set on Vendor Merge Screen as:", BaseClass.getattribute(vendorMergeAltVendorId, "value"));
		clickSearch();
		verifyTotalRecords(BaseClass.getqueryresult("Select count(*) from vendor where VendorNumber like '%"+searchableField+"%'").get(0));
	}
	public void searchByFEIN() {
		// TODO Auto-generated method stub
		
	}
	public void searchByAddress() {
		// TODO Auto-generated method stub
		
	}
	public void searchByZIP() {
		// TODO Auto-generated method stub
		
	}
	
	
}
