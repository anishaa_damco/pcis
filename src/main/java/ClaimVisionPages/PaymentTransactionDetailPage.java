package ClaimVisionPages;

import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class PaymentTransactionDetailPage {
	
	private PaymentTransactionDetailPage(){}
	private static final PaymentTransactionDetailPage PTDSPage=new PaymentTransactionDetailPage();

	public static PaymentTransactionDetailPage getinstance()
	{
		return PTDSPage;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="TransactionHistory_ExcludeCyclePaymentsCheckBox")
	WebElement excludeCycleCheckBox;
	
	@FindBy(id="TransactionHistory_ClaimPaymentCycleTextBox")
	WebElement cycleID; 
	
	@FindBy(id="TransactionHistory_paymentNumberTextBox")
	WebElement paymentID; 
	
	@FindBy(id="Claim_saveMessageLabel")
	WebElement messageLabel; 
	
	@FindBy(id="TransactionHistory_claimNumberTextBox")
	WebElement claimNumber;
	
	@FindBy(id="TransactionHistory_customizeButton")
	WebElement customize;
	
	@FindBy(id="SearchCustomization1_removeAllButton")
	WebElement removeAll;
	
	@FindBy(id="SearchCustomization1_availableListBox")
	WebElement availableList;
	
	@FindBy(id="SearchCustomization1_addButton")
	WebElement addbutton;
	
	@FindBy(id="SearchCustomization1_saveButton")
	WebElement saveButton;
	
	@FindBy(id="TransactionHistory_searchButton")
	WebElement search;
	
	@FindBy(id="TransactionHistory_CustomizationGrid1_searchResultDiv")
	WebElement searchResultTable;
	
	@FindBy(id="TransactionHistory_paymentPager_lblResults")
	WebElement resultCount;
	
	@FindBy(xpath="//table[@id='TransactionHistory_CustomizationGrid1_CustomGridView']//tr/td[4]")
	List<WebElement> transactionStatus;
	
    private void setColumn()
    {
    	String parent=BaseClass.getcurrentwindow();
    	BaseClass.click(customize);
    	BaseClass.switchToNextWindow(parent, "Customize My View", 2);
    	BaseClass.click(removeAll);
    	Wait.waitFor(5);
    	BaseClass.selectByVisibleText(availableList, "Claim #");
    	BaseClass.selectByVisibleText(availableList, "Payment #");
    	BaseClass.selectByVisibleText(availableList, "Pay Amount");
    	BaseClass.selectByVisibleText(availableList, "Pay Status");
    	BaseClass.click(addbutton);
    	Wait.waitFor(5);
    	BaseClass.click(saveButton);
    	BaseClass.closeWindow();
    	BaseClass.switchToWindow(parent, 1);
    }
    
    public void searchPTDS(String PaymentID)
    {
    setColumn();
    BaseClass.settext(paymentID, PaymentID);
    }   
      
    public void verifyPayment(HashMap<String, String> hm)
    {
    	DashboardPage.getinstance().navigateToPTDS();
    	setColumn();
    	Assert.assertTrue(BaseClass.getattribute(claimNumber, "value").equals(hm.get("ClaimNumber")));
    	
    	if(hm.get("PaymentType").equals("Recurring"))
    	{
    		if(excludeCycleCheckBox.isSelected())
    		{
    			BaseClass.click(excludeCycleCheckBox);
    			BaseClass.logInfo("Exclude Cycle Payment is deselected", "");
    		}
    		BaseClass.settext(cycleID, hm.get("ClaimPaymentID"));
			BaseClass.logInfo("CycleID is set as", BaseClass.getattribute(cycleID, "value"));
    	}
    	else
    	{
    	BaseClass.settext(paymentID, hm.get("ClaimPaymentID"));
		BaseClass.logInfo("PaymentID is set as", BaseClass.getattribute(paymentID, "value"));
    	}
    	BaseClass.click(search);
		BaseClass.logInfo("Search Button is Clicked", "");
    	Wait.waitforelement(BaseClass.getDriver(), searchResultTable);
    	int transactionCount=0;
    	if(hm.get("PaymentType").equals("Recurring"))
    	{
    		transactionCount=Integer.parseInt(BaseClass.getqueryresult("Select count(*) from ClaimTransaction where ClaimPaymentId in (select claimpaymentid from ClaimPayment where ClaimPaymentCycleId='"+hm.get("ClaimPaymentID")+"')").get(0));
    	}
    	else
    	{
    		transactionCount=Integer.parseInt(BaseClass.getqueryresult("Select count(*) from ClaimTransaction where ClaimPaymentId ='"+hm.get("ClaimPaymentID")+"'").get(0));
    	}
    	Assert.assertEquals(BaseClass.gettext(resultCount), "Results 1 - "+transactionCount+" of "+transactionCount);
    	BaseClass.logpass("Total Count should be displayed as "+transactionCount, "Total Count are displayed as "+transactionCount);
    	for(int i=0;i<transactionCount;i++)
    	{
    		
    		Assert.assertEquals(BaseClass.gettext(transactionStatus.get(0)), hm.get("PaymentStatus"));
    	}
    	BaseClass.logpass("All Transaction status should be "+hm.get("PaymentStatus"), "All Transaction status is "+hm.get("PaymentStatus"));
    }
    
    public void verifyVoidedPayment(String ClaimPaymentID)
    {
    	DashboardPage.getinstance().navigateToPTDS();
    	setColumn();
    	BaseClass.settext(paymentID, ClaimPaymentID);
		BaseClass.logInfo("PaymentID is set as", BaseClass.getattribute(paymentID, "value"));
    	BaseClass.click(search);
		BaseClass.logInfo("Search Button is Clicked", "");
    	Wait.waitforelement(BaseClass.getDriver(), searchResultTable);
    	
		int transactionCount=Integer.parseInt(BaseClass.getqueryresult("Select count(*) from ClaimTransaction where ClaimPaymentId ='"+ClaimPaymentID+"'").get(0));
		Assert.assertEquals(BaseClass.gettext(resultCount), "Results 1 - "+transactionCount+" of "+transactionCount);
    	BaseClass.logpass("Total Count should be displayed as "+transactionCount, "Total Count are displayed as "+transactionCount);
    	for(int i=0;i<transactionCount;i++)
    	{
    		
    		Assert.assertEquals(BaseClass.gettext(transactionStatus.get(0)), "Voided");
    	}
    	BaseClass.logpass("All Transaction status should be Voided", "All Transaction status is Voided");
    }
    
}
