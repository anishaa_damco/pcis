package ClaimVisionPages;

import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class ReleasePaymentPage {
	private ReleasePaymentPage(){}
	private static final ReleasePaymentPage releasepaymentPage=new ReleasePaymentPage();

	public static ReleasePaymentPage getinstance()
	{
		return releasepaymentPage;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="ReleasePayments_adjusterDropDownList")
	WebElement adjuster;
	
	@FindBy(id="ReleasePayments_claimNumberTextBox")
	WebElement claimNumber;
	
	@FindBy(id="ReleasePayments_dateFromTextBox")
	WebElement releaseDateFrom;
	
	@FindBy(id="ReleasePayments_dateToTextBox")
	WebElement releaseDateTo;
	
	@FindBy(id="ReleasePayments_payCatDropDownList")
	WebElement payCategory;
	
	@FindBy(id="ReleasePayments_paySubCatDropDownList")
	WebElement paySubCategory;
	
	@FindBy(id="ReleasePayments_noSearchResultLabel")
	WebElement noResultMessage;
	
	@FindBy(id="ReleasePayments_searchButton")
	WebElement search;
	
	@FindBy(id="ReleasePayments_clearButton")
	WebElement clear;
	
	@FindBy(id="ReleasePayments_selectAllButton")
	WebElement selectAll;
	
	@FindBy(id="ReleasePayments_clearAllButton")
	WebElement clearAll;
	
	@FindBy(id="ReleasePayments_SaveButton")
	WebElement save;
	
	@FindBy(id="ReleasePayments_DeleteButton")
	WebElement delete;
	
	@FindBy(xpath="//input[@type='checkbox']")
	List<WebElement> paymentcheckboxes;
	
	@FindBy(id="ReleasePayments_paymentPager_lblResults")
	WebElement records;

	public void verifySelectAll() {
		String claimnumber=BaseClass.getqueryresult("Select TOP 1 ClaimNumber from Claimpayment where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Pending' )	 order by ClaimNumber").get(0);
		BaseClass.settext(claimNumber, claimnumber);
		BaseClass.click(search);
		BaseClass.logInfo("Search Button is clicked","");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		BaseClass.click(selectAll);
		BaseClass.logInfo("Select All Button is clicked","");
		Wait.waitFor(2);
		for(int i=0;i<paymentcheckboxes.size();i++)
		{
			BaseClass.Scrolltoelement(paymentcheckboxes.get(i));
			Assert.assertTrue(paymentcheckboxes.get(i).isSelected());
		}
		BaseClass.logpass("All Payments should be Selected", "All Payments are Selected");
		
	}
	
	public void seachByAdjuster() {
		for(int i=1;i<BaseClass.getAllOptions(adjuster).size();i++)
		{
			try
			{
				BaseClass.selectByIndex(adjuster, i);
			BaseClass.click(search);
			Wait.waitFortextPresent(BaseClass.getDriver(), records);
			BaseClass.logInfo("Adjuster is selected as", BaseClass.getfirstselectedoption(adjuster));
			BaseClass.logInfo("Search Button is clicked","");
			break;
			}
			catch(Exception e)
			{
			}
			
		}
		Assert.assertTrue(BaseClass.gettext(records).contains("Results 1 - "));
	       BaseClass.logpass("Payments should be displayed", "Payments are displayed");
	}
	
	
	public void verifyDeselectAll() {
		String claimnumber=BaseClass.getqueryresult("Select TOP 1 ClaimNumber from Claimpayment where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Pending' )	 order by ClaimNumber").get(0);
		BaseClass.settext(claimNumber, claimnumber);
		BaseClass.click(search);
		BaseClass.logInfo("Search Button is clicked","");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		BaseClass.click(selectAll);
		BaseClass.logInfo("Select All Button is clicked","");
		Wait.waitFor(2);
		BaseClass.click(clearAll);
		BaseClass.logInfo("Clear All Button is clicked","");
		Wait.waitFor(2);
		for(int i=0;i<paymentcheckboxes.size();i++)
		{
			BaseClass.Scrolltoelement(paymentcheckboxes.get(i));
			Assert.assertFalse(paymentcheckboxes.get(i).isSelected());
		}
		BaseClass.logpass("All Payments should be Deselected", "All Payments are Deselected");
		
	}
	
	public void searchWithClaimNumber()
	{   String claimnumber=BaseClass.getqueryresult("Select TOP 1 ClaimNumber from Claimpayment where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Pending' )	 order by ClaimNumber").get(0);
		BaseClass.settext(claimNumber, claimnumber);
		BaseClass.click(search);
		BaseClass.logInfo("Search Button is clicked","");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		String count=BaseClass.getqueryresult("Select count(*) from Claimpayment where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Pending' )	and claimnumber='"+claimnumber+"'").get(0);
       Assert.assertTrue(BaseClass.gettext(records).equals("Results 1 - "+count+" of "+count));
       BaseClass.logpass("Total Payments should be "+count, "Total Payments are "+count);
	}
	
	public void searchWithReleaseDate()
	{  
		BaseClass.settext(releaseDateFrom, BaseClass.pastdatefromTodayDate(300));
		BaseClass.settext(releaseDateTo, BaseClass.getCurrentDate());
		BaseClass.click(search);
		BaseClass.logInfo("Search Button is clicked","");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		String count=BaseClass.getqueryresult("Select count(*) from Claimpayment where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Pending' )	and FromDate>='"+BaseClass.getattribute(releaseDateFrom, "value")+"' and ThroughDate<='"+BaseClass.getattribute(releaseDateTo, "value")+"'").get(0);
       Assert.assertTrue(BaseClass.gettext(records).equals("Results 1 - "+count+" of "+count));
       BaseClass.logpass("Total Payments should be "+count, "Total Payments are "+count);
	}
	
	public void searchWithPaymentCategory()
	{  
		List<String> paymentCategoryData=BaseClass.getqueryresult("Select TOP 1 pc.Description,psc.Description from ClaimPaymentDetail as cpd JOIN Finance.PaymentCategory as pc ON  cpd.PaymentCategoryId=pc.PaymentCategoryId JOIN Finance.PaymentSubCategory psc ON cpd.PaymentSubCategoryId=psc.PaymentSubCategoryId where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Released' )");
		BaseClass.selectByVisibleText(payCategory, paymentCategoryData.get(0));
		Wait.waitforOptions(paySubCategory);
		BaseClass.selectByVisibleText(paySubCategory, paymentCategoryData.get(1));
		BaseClass.click(search);
		BaseClass.logInfo("Search Button is clicked","");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		//String count=BaseClass.getqueryresult("Select count(*) from Claimpayment where PaymentStatusID = (Select PaymentStatusId from PaymentStatus	where PaymentStatusCode='Pending' )	and claimnumber='"+claimnumber+"'").get(0);
		Assert.assertTrue(BaseClass.gettext(records).contains("Results 1 - "));
	       BaseClass.logpass("Payments should be displayed", "Payments are displayed");
	}
	
	public void releasePayment(String ClaimNumber,String PaymentID)
	{
		BaseClass.settext(claimNumber, ClaimNumber);
		BaseClass.click(search);
		BaseClass.logInfo("Search Button is clicked","");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		if(PaymentID.equals(""))
		{
			BaseClass.click(selectAll);
			BaseClass.logInfo("Select All Button is clicked","");
			Wait.waitFor(2);
		}
		else
		{
		   BaseClass.click(BaseClass.getDriver().findElement(By.xpath("//span[contains(text(),'"+PaymentID+"')]/parent::td/parent::tr//input")));
		}
		String totalrecords=BaseClass.gettext(records);
		BaseClass.click(save);
		BaseClass.logInfo("Save Button is clicked","");
		if(PaymentID.equals(""))
		{
			Wait.waitFortextPresent(BaseClass.getDriver(), noResultMessage);
			Assert.assertTrue(BaseClass.gettext(noResultMessage).equals("No results returned."));
			BaseClass.logpass("All Payments should be Released Successfully", "All Payments are Released Successfully");
		}
		else
		{
			Wait.waitForTextChanged(records, totalrecords);
		   BaseClass.logpass("Payment should be saved", "Payment is saved successfully");
		}
	}
	@FindBy(id="ReleasePayments_paymentReleaseGridView_ctl02_ReleaseCheckBox")
	WebElement firstPaymentCheckBox;
	@FindBy(id="ReleasePayments_paymentReleaseGridView_ctl02_PaymentIdLabel")
	WebElement firstPaymentID;
	public void deletePayment(String ClaimNumber,String PaymentID)
	{
	    
		BaseClass.settext(claimNumber, ClaimNumber);
		BaseClass.click(search);
		BaseClass.logInfo("Search Button is clicked","");
		Wait.waitFortextPresent(BaseClass.getDriver(), records);
		if(PaymentID.equals(""))
		{
			BaseClass.click(firstPaymentCheckBox);
			PaymentID=BaseClass.gettext(firstPaymentID);
			
		}
		else
		{
		   BaseClass.click(BaseClass.getDriver().findElement(By.xpath("//span[contains(text(),'"+PaymentID+"')]/parent::td/parent::tr//input")));
		}
		String totalrecords=BaseClass.gettext(records);
		BaseClass.click(delete);
		BaseClass.logInfo("Delete Button is clicked","");
		try {
			Thread.sleep(5000);
			BaseClass.getDriver().switchTo().alert().accept();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Wait.waitForTextChanged(records, totalrecords);
		String count=BaseClass.getqueryresult("Select count(*) from ClaimPayment where ClaimPaymentID="+PaymentID).get(0);
        Assert.assertEquals(count, "0");
		
		}
	
	
	

}
