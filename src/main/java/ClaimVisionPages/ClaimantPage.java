package ClaimVisionPages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class ClaimantPage {
	
	private ClaimantPage(){}
	private static final ClaimantPage claimantPg=new ClaimantPage();

	public static ClaimantPage getinstance()
	{
		return claimantPg;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="Claim_saveButton")
	WebElement claimSave; 
	
	@FindBy(id="Claim_saveMessageLabel")
	WebElement messageLabel; 
	
	@FindBy(css = "select#Claim_statusDropDown")
	WebElement status;
	
	@FindBy(css="input#Claim_saveButton")
	WebElement saveBtn;
	
	@FindBy(css="input#saveClaimImageButton")
	WebElement saveIcon;
	
	@FindBy(css = "input#openClaimImageButton")
	WebElement openClaimImageButton;
	
	@FindBy(css = "input#closeClaimImageButton")
	WebElement closeClaimIcon;
	//@FindBy(css ="div#claim_claimInfo_claimInfoPanel > table>tbody>tr")
	
	//@FindBy(css = "div#Claimants_claimInfo_claimInfoPanel > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	
	
	@FindBy(css = "div[id$='_claimInfo_claimInfoPanel'] > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	//"input[id$='_closeCheckBox']"
	WebElement claimStatusLabel;
	String claimantPage,DiaryPage;
	
	@FindBy(css = "input#Claimants_claimantCommentTextBox")
	WebElement claimantComment;
	
	@FindBy(css = "input#Claimants_damagesTextBox")
	WebElement claimantDamages;
	
	@FindBy(css = "input#Claimants_summaryTextBox")
	WebElement claimantSummary;
	
	@FindBy(css="span#Claimants_claimantCommentDisplayCharCount_countLabel> span")
	public WebElement claimantCommentCountLabel;
	
	@FindBy(css="span#Claimants_summaryDisplayCharCount_countLabel > span")
	public WebElement claimantSummaryCountLabel;
	
	@FindBy(css="span#Claimants_damagesDisplayCharCount_countLabel > span")
	public WebElement claimantDamagesCountLabel;
	
	
	@FindBy(css = "span#Claim_ClaimantTotal1_OSReservesLabel")
	WebElement osReserve;
	
	
	@FindBy(css="input#openClaimImageButton")
	WebElement openClaimIcon;
	
	@FindBy(css = "select#Claim_statusDropDown")
	WebElement statusDropDown;
	
	@FindBy(id="btnClose")
	WebElement closedBtnForOpenDiaries;
	
	@FindBy(css = "input#Claim_cancelButton")
	WebElement cancel;
	
	@FindBy(css= "input#Claimants_AddPartyButton")
	WebElement addClaimantBtn;
	
	@FindBy(css = "select#Claimants_claimantTypeDropDown")
	private WebElement claimantDropDown;
	
	@FindBy(css = "input#Claimants_PartyContactControl_ClaimantFirstNameTextBox")
	private WebElement claimantFirstName;

	@FindBy(css = "input#Claimants_PartyContactControl_ClaimantMiddleNameTextBox")
	private WebElement claimantMiddleName;

	@FindBy(css = "input#Claimants_PartyContactControl_ClaimantLastNameTextBox")
	private WebElement claimanLastName;
	
	@FindBy(css="input#Claimants_PartyContactControl_AddressControl1_ZipCodeTextBox")
	private WebElement zip;
	
	@FindBy(css="input#Claimants_reportDateToInsuredTextBox")
	private WebElement reportDateToInsured;
	
	@FindBy(css = "input#Claimants_incidentDateText")
	private WebElement incidentDate;

	
	@FindBy(css="input#Claimants_PartyContactControl_AddressControl1_Address1TextBox")
	private WebElement address;
	
	@FindBy(css="select#Claimants_PartyContactControl_AddressControl1_CityDropDownList")
	private WebElement city;
	
	@FindBy(css="select#Claimants_PartyContactControl_AddressControl1_StateDropDownList")
	private WebElement state;
	
	@FindBy(css="input#Claimants_PartyContactControl_AddressControl1_countyTextBox")
	private WebElement county;

	
	@FindBy(css= "input#Claimants_InsuredControl_insuredNameTextBox")
	public WebElement insuredName;
	
	@FindBy(css="input#Claimants_InsuredControl_streetLine1TextBox")
	public WebElement streetLine1Insured;
	
	@FindBy(css="input#Claimants_InsuredControl_streetLine2TextBox")
	public WebElement streetLine2Insured;
	
	@FindBy(css="input#Claimants_InsuredControl_zipTextBox")
	public WebElement zipInsured;
	
	@FindBy(css= "select#Claimants_claimantDescriptionDropDown")
	public WebElement claimantDecsription;
	
	
	@FindBy(css= "input#Claimants_PartyContactControl_IsCompanyClaimantCheckBox")
	public WebElement claimantAsCompanyCheckbox;
		
	@FindBy(css= "input#Claimants_PartyContactControl_ClaimantCompanyTextBox")
	public WebElement claimantAsCompanyTextbox;
	
	
	
	public void openStatusOfClaim(HashMap<String, String> hm){
		if(hm.get("StatusChange").contains("Icon")){
			BaseClass.click(openClaimIcon);
			BaseClass.logInfo("Clicked on Open claim icon", "");
			BaseClass.acceptalert();
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			BaseClass.logInfo("Alert accepted for opening the claim", "");
			
		}
		
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains( "Open"));
		BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
	
	}
	
	public HashMap<String, String>  getTotalAmountListClaimScreen(){
		HashMap<String, String> hm= new HashMap<String, String>();;
		List<HashMap> list = new ArrayList<HashMap>();
		//*[@id="Claim_closingclaimdiv"]/table[2]/tbody/tr[10]/td/div/fieldset/table/tbody/tr[2]
		//int lastRowcount=BaseClass.getDriver().findElements(By.xpath("//*[@id='FinSum_FinancialSummaryGridView']/tbody/tr")).size();
		//reading row which is having total amount of each column
		List<WebElement> allColumnOfClaimFinancials =BaseClass.getDriver().findElements(By.xpath("//*[@id='Claim_closingclaimdiv']/table[2]/tbody/tr[10]/td/div/fieldset/table/tbody/tr[2]/td"));
		List<WebElement> allColumnHeading =BaseClass.getDriver().findElements(By.xpath("//*[@id='Claim_closingclaimdiv']/table[2]/tbody/tr[10]/td/div/fieldset/table/tbody/tr[1]/th"));
	     int colsize = allColumnOfClaimFinancials.size();
	     int colheadsize = allColumnHeading.size();
	     for(;colsize>1;colsize--,colheadsize--)
	     {
	    	 hm.put(allColumnHeading.get(colheadsize-1).getText(), allColumnOfClaimFinancials.get(colsize-1).getText()) ;
	     }
	   
	    	
	    	
	   return hm;
		
	}
	
	public void reClosedStatusOfClaim(HashMap<String, String> hm){
		
		//claimantPage=BaseClass.getcurrentwindow();
		DashboardPage.getinstance().navigateToClaim();
		Wait.waitForTitle(BaseClass.getDriver(), "Claim");
		BaseClass.logInfo("Navigate to Claim Screen", "");
		HashMap<String, String> financialTotal = getTotalAmountListClaimScreen();
		System.out.println(financialTotal);
		DashboardPage.getinstance().navigateToClaimant(hm,getClaimStatus());
		//BaseClass.getDriver().switchTo().window(claimantPage);
		//BaseClass.switchToWindow(claimantPage,1);
		Wait.waitForTitle(BaseClass.getDriver(), "Claimants");
		BaseClass.logInfo("Navigate to Claimant Screen", "");
		System.out.println("hm: "+Arrays.asList(hm));
		if(hm.get("StatusChange").contains("Icon")){
			
			BaseClass.click(closeClaimIcon);
			BaseClass.logInfo("Clicked on close claim icon", "");
			
			
		}
		BaseClass.acceptalert();
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		BaseClass.logInfo("Alert for closing the claim is accepted", "");
		
		
		if((!(financialTotal.get("Paid").contains("$0.00"))
				|| (!financialTotal.get("OS Reserves").contains("$0.00"))))
		{
			//outstanding reserves error message will display
			//need to put assert condition
			BaseClass.logInfo("Getting outstanding reserve error message while closing the claim", "");
			BaseClass.logInfo("Claim having OS Reserve amount: ", financialTotal.get("OS Reserves"));
			BaseClass.logInfo("Claim having Paid amount: ", financialTotal.get("Paid"));
		}
		
		else if ((!financialTotal.get("Pending").contains("$0.00"))
				|| hm.get("PaymentStatus").equals("7")
				||((!financialTotal.get("Paid").contains("$0.00"))
						&&(financialTotal.get("Pending").contains("$0.00")) 
								&& (financialTotal.get("OS Reserves").contains("$0.00")))){
			//pending error message will display
			//need to put assert condition
			System.out.println("Entered in Pending condition verification");
			BaseClass.logInfo("Getting pending payment error message while closing the claim", "");
			BaseClass.logInfo("Claim having Pending amount: ", financialTotal.get("Pending"));
		}
		else {
			//open diaries pop-up handling
			if(BaseClass.getDriver().getWindowHandles().size()>1)
			{
				DiaryPage=BaseClass.switchToNextWindow(claimantPage,"Open Diaries", 2);
				BaseClass.logInfo("Diary Page is displayed", "");
				BaseClass.click(closedBtnForOpenDiaries);
				BaseClass.logInfo("Diaries pop-up is closed successfully", "");
				BaseClass.switchToWindow(claimantPage,1);
			}
			
			Assert.assertEquals(BaseClass.gettext(messageLabel), "The claim was closed successfully.");
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			System.out.println(BaseClass.gettext(claimStatusLabel));
			Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains("Closed")) ;
			BaseClass.logInfo("Successfully closed the claim", "Successfully closed the claim");
		}
	
	}
	
	

	public void claimStatusVerification(HashMap<String, String> hm)
	{
		if(hm.get("Scenario").contains("ReOpened")){
			if(BaseClass.gettext(claimStatusLabel).contains("Closed") ||
					BaseClass.gettext(claimStatusLabel).contains("ReClosed"))
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-opened status", "");
				}
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-opened status", "");
				}
			}
			BaseClass.logpass("Successfully reOpened the claim", "Successfully reOpened the claim");
				
		}
		else if(hm.get("Scenario").contains("ReClosed") ){
			if(BaseClass.gettext(claimStatusLabel).contains("ReOpened")){
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Closed")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-closed status", "");
				}
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-closed status", "");
				}
				reClosedStatusOfClaim(hm);
			}
			BaseClass.logpass("Successfully reClosed the claim", "Successfully reClosed the claim");
		}
		
		else if(hm.get("Scenario").contains("Closed")){
			if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				
			}
			BaseClass.logpass("Successfully Closed the claim", "Successfully Closed the claim");
		}
		else if(hm.get("Scenario").contains("Open")){
			openStatusOfClaim(hm);
			}
		BaseClass.logpass("Successfully Open the claim", "Successfully Open the claim");
	}
	
	
	/*public void claimScreen(HashMap<String, String> hm)
	{
		System.out.println(BaseClass.gettext(claimStatus) + "before changing the status");
		if(hm.get("Scenario").equals("Open")){
			BaseClass.selectByVisibleText(status, hm.get("Scenario"));
			System.out.println(BaseClass.getfirstselectedoption(status));
		}
		else if (hm.get("Scenario").equals("Closed")){
			BaseClass.selectByVisibleText(status, hm.get("Scenario"));
			System.out.println(BaseClass.getfirstselectedoption(status));
			}
		else if (hm.get("Scenario").equals("ReClosed"))
			BaseClass.click(closeClaimImageButton);
		else if	(hm.get("Scenario").equals("ReOpened")) 
			BaseClass.click(openClaimImageButton);
		
		//BaseClass.acceptalert();
		
		if(hm.get("Save").equals("Button"))
			BaseClass.click(saveBtn);
		else if(hm.get("Save").equals("Icon"))
			BaseClass.click(saveIcon); 
		System.out.println(BaseClass.gettext(osReserve));
		System.out.println(BaseClass.gettext(claimStatus) + "after status change");
		//accepting outstanding reserve pop-up if any reserves exists
		if(!BaseClass.gettext(osReserve).equals("$0.00") &&
				(hm.get("Scenario").equals("ReClosed") ||
						hm.get("Scenario").equals("Closed")))
			BaseClass.acceptalert();
       
        	
		System.out.println(hm.get("Scenario"));
		BaseClass.logInfo("Status is selected a"
				+ "s", BaseClass.getfirstselectedoption(status));
		Assert.assertTrue(BaseClass.gettext(claimStatus).equals(hm.get("Scenario")));
		//Assert.assertTrue(BaseClass.getfirstselectedoption(status).equals(hm.get("Scenario")));
	}
	*/
	
	public void cancelButtonVerify() {
		String incidentDes = null;
		incidentDes =BaseClass.getattribute(claimantSummary, "value"); 
		BaseClass.logInfo("Claimant Summary before entering new data: ", incidentDes);
		BaseClass.settext(claimantSummary, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered the Claimant Summary : ", BaseClass.getattribute(claimantSummary, "value"));
		//BaseClass.logInfo("Incident description length after entering data: ", BaseClass.getattribute(incidentDescCountLabel, "value"));
		BaseClass.click(cancel);
		BaseClass.logInfo("Clicked on cancel button", "");
		Wait.waitforelement(BaseClass.getDriver(), claimantSummary);
		Wait.waitForContainValue(claimantSummary, incidentDes);
		BaseClass.logInfo("Claimant Summary after clicked on cancel button: ", BaseClass.getattribute(claimantSummary, "value"));
		Assert.assertEquals(incidentDes, BaseClass.getattribute(claimantSummary, "value"));
		BaseClass.logpass("Verified cancel button is working as expected","");
		
	}
	public void limitVerification(String Scenario) {
		if(Scenario.equals("Claimant Comment")){
			BaseClass.settext(claimantComment, BaseClass.stringGeneratorl(75));
			BaseClass.logInfo("Entered claimant Comment as : ", BaseClass.getattribute(claimantComment, "value"));
			String countLabelValue =BaseClass.gettext(claimantCommentCountLabel);
			BaseClass.logInfo("Claimant Comment length is: ", countLabelValue);
			Assert.assertTrue(countLabelValue.contains("75/75"));
			BaseClass.logInfo("Not able to enter Claimant Comment more than it's length", "");
		}
		else if (Scenario.equals("Claimant Summary"))
		{
			BaseClass.settext(claimantSummary, BaseClass.stringGeneratorl(250));
			BaseClass.logInfo("Entered claimant summary as : ", BaseClass.getattribute(claimantSummary, "value"));
			String countLabelValue =BaseClass.gettext(claimantSummaryCountLabel);
			BaseClass.logInfo("Claimant summary length is : ", countLabelValue);
			Assert.assertTrue(countLabelValue.contains("250/250"));
			BaseClass.logInfo("Not able to enter Claimant summary more than it's length", "");
		}
		else if (Scenario.equals("Claimant Damages"))
		{
			BaseClass.settext(claimantDamages, BaseClass.stringGeneratorl(250));
			BaseClass.logInfo("Entered claimant damages as : ", BaseClass.getattribute(claimantDamages, "value"));
			String countLabelValue =BaseClass.gettext(claimantDamagesCountLabel);
			BaseClass.logInfo("Claimant damages length is : ", countLabelValue);
			Assert.assertTrue(countLabelValue.contains("250/250"));
			BaseClass.logInfo("Not able to enter Claimant damages more than it's length", "");
		}
		BaseClass.click(claimSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
		
	}
	public Boolean getClaimStatus() {
		
		Boolean status = BaseClass.gettext(claimStatusLabel).contains("Closed");
		return status;
	}
	public void addClaimant(HashMap<String, String> hm) {
		// TODO Auto-generated method stub
		
		BaseClass.click(addClaimantBtn);
		BaseClass.logInfo("Clicked on Add Claimant Button", "");
		if(hm.get("ClaimantType").equals("Claimant"))
		{
			BaseClass.selectByVisibleText(claimantDropDown, "Claimant");
			BaseClass.logInfo("Claimant type as Claimant is selected", "");
			int random = BaseClass.generateRandomNumberAsInteger(2);
			
			if(random==0){
			BaseClass.settext(claimantFirstName, BaseClass.stringGeneratorl(4));
			BaseClass.logInfo("First name of claimant is entered as:", BaseClass.getattribute(claimantFirstName, "value"));
			BaseClass.settext(claimanLastName, BaseClass.stringGeneratorl(5));
			BaseClass.logInfo("Last name of claimant is entered as:", BaseClass.getattribute(claimanLastName, "value"));
			}
			else if (random == 1)
			{
				BaseClass.click(claimantAsCompanyCheckbox);
				BaseClass.logInfo("Clamant as companycheckbox is selected", "");
			}
			BaseClass.settext(address, BaseClass.stringGeneratorl(8));
			BaseClass.logInfo("Address of claimant is entered as:", BaseClass.getattribute(address, "value"));
			BaseClass.settext(zip, "22222");
			BaseClass.logInfo("zip of claimant is entered as:", BaseClass.getattribute(zip, "value"));
			BaseClass.settext(reportDateToInsured, BaseClass.futuredatefromdate(incidentDate.toString(), 2));
		}
		
		else if(hm.get("ClaimantType").equals("Insured"))
		{
			
		}
		
		BaseClass.click(claimSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
	
	}
	
	
	
	
}
