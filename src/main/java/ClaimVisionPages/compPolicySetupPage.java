package ClaimVisionPages;

import java.util.HashMap;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import CommonModules.BaseClass;

public class compPolicySetupPage {
	
	private compPolicySetupPage(){}
	private static final compPolicySetupPage policySetup=new compPolicySetupPage();
	
	public static compPolicySetupPage getinstance()
	{
		return policySetup;
	}
	
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id = "Policy_addButton")
	public WebElement addPolicy;
	
	

	public void AddPolicy(HashMap<String, String> hm) {
		// TODO Auto-generated method stub
		BaseClass.click(addPolicy);
		BaseClass.logInfo("Clicked on Add policy button", "");
		
	}

}
