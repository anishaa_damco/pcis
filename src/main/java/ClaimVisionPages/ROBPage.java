package ClaimVisionPages;

import java.util.HashMap;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class ROBPage {
	private static final ROBPage robPage=new ROBPage();

	public static ROBPage getinstance()
	{
		return robPage;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="RecipientOfBenefit_addButton")
	WebElement addROB;
	@FindBy(id="RecipientOfBenefit_robTypeDropDown")
	WebElement robType;
	
	@FindBy(id="RecipientOfBenefit_emailTextBox")
	WebElement robEmail;
	
	@FindBy(id="RecipientOfBenefit_firstNameTextBox")
	WebElement robFirstName;
	
	@FindBy(id="RecipientOfBenefit_lastNameTextBox")
	WebElement robLastName;
	

	@FindBy(id="RecipientOfBenefit_miTextBox")
	WebElement robMiddleName;
	

	@FindBy(id="RecipientOfBenefit_phoneNumberTextBox")
	WebElement robPhoneNumber;
	
	@FindBy(id="RecipientOfBenefit_companyNameTextBox")
	WebElement robCompanyName;
	
	@FindBy(id="RecipientOfBenefit_saveButton")
	WebElement saveROB;
	
	@FindBy(id="RecipientOfBenefit_saveMessageLabel")
	WebElement successMessage;
	@FindBy(id="RecipientOfBenefit_validationSummary")
	WebElement errorMessage;
	
	@FindBy(id="RecipientOfBenefit_foreignAddressCheckBox")
	WebElement isForeignAddressCheckBox;
	@FindBy(id="RecipientOfBenefit_streetLine1TextBox")
	WebElement address1;
	@FindBy(id="RecipientOfBenefit_streetLine2TextBox")
	WebElement address2;
	@FindBy(id="RecipientOfBenefit_zipTextBox")
	WebElement postal;
	@FindBy(id="RecipientOfBenefit_CityDropDownList")
	WebElement city;
	@FindBy(id="RecipientOfBenefit_stateDropDown")
	WebElement state;
	@FindBy(id="RecipientOfBenefit_foreignAddress1TextBox")
	WebElement foreignAddress1;
	@FindBy(id="RecipientOfBenefit_foreignAddress2TextBox")
	WebElement foreignAddress2;
	@FindBy(id="RecipientOfBenefit_ForeignCity")
	WebElement foreignCity;
	@FindBy(id="RecipientOfBenefit_ForeignCounty")
	WebElement foreignCounty;
	@FindBy(id="RecipientOfBenefit_ForeignStateDDL")
	WebElement foreignState;
	@FindBy(id="RecipientOfBenefit_ForeignCountryDDL")
	WebElement foreignCountry;
	@FindBy(id="RecipientOfBenefit_ForeignZip")
	WebElement foreignZip;
	@FindBy(id="RecipientOfBenefit_ssnTextBox")
	WebElement SSN;
	@FindBy(id="RecipientOfBenefit_robDobTextBox")
	WebElement DOB;
	@FindBy(id="RecipientOfBenefit_sexRadioList_0")
	WebElement male;
	@FindBy(id="RecipientOfBenefit_sexRadioList_1")
	WebElement female;
	@FindBy(id="RecipientOfBenefit_raceDropDown")
	WebElement race;
	@FindBy(id="RecipientOfBenefit_guardianTextBox")
	WebElement guardian;
	@FindBy(id="RecipientOfBenefit_relationshipDropDown")
	WebElement relationship;
	@FindBy(id="RecipientOfBenefit_dependencyRadioList_0")
	WebElement total;
	@FindBy(id="RecipientOfBenefit_dependencyRadioList_1")
	WebElement partial;
	@FindBy(id="RecipientOfBenefit_schoolingStatusDropDown")
	WebElement schoolingStatus;
	@FindBy(id="RecipientOfBenefit_grossProceedsCheckBox")
	WebElement attornyGrossProceed;
	@FindBy(id="RecipientOfBenefit_CancelButton")
	WebElement cancel;
		
	public void createROB()
	{
		for(int i=0;i<2;i++)
		{
		   BaseClass.click(addROB);
		   BaseClass.Scrolltoelement(robType);
		   if(i==0)
		   {
		   BaseClass.selectByVisibleText(robType, "Dependent");
		   BaseClass.settext(robFirstName, BaseClass.stringGeneratorl(5));
		   BaseClass.settext(robLastName, BaseClass.stringGeneratorl(5));
		   }
		   else
		   {
			   BaseClass.selectByVisibleText(robType, "Special Payee");
			   BaseClass.settext(robCompanyName, BaseClass.stringGeneratorl(5) +" "+BaseClass.stringGeneratorl(5));
		   }
		   BaseClass.click(saveROB);
           Wait.waitFortextPresent(BaseClass.getDriver(), successMessage);
           Assert.assertEquals(BaseClass.gettext(successMessage), "Saved successfully.");
		}
		BaseClass.logInfo("ROB data is added successfully","");
	}
	
	public void createROB(HashMap<String,String> hm)
	{
		   BaseClass.click(addROB);
		   BaseClass.logInfo("Add Button is clicked", "");
		   BaseClass.Scrolltoelement(robType);
		   if(hm.get("Scenario").equals("Mandatory"))
		   {
			   BaseClass.click(saveROB);
			   BaseClass.logInfo("Save Button is clicked", "");
	           Wait.waitFortextPresent(BaseClass.getDriver(), errorMessage);
	           Assert.assertEquals(BaseClass.gettext(errorMessage), "Please select a ROB Type.\nFirst Name is required.\nLast Name is required.");
	           BaseClass.logpass("Error Message should be displayed for Mandatory Fields", "Error Message is displayed for MandatoryFields as: Please select a ROB Type.\nFirst Name is required.\nLast Name is required.");
	           BaseClass.selectByVisibleText(robType, "Dependent");
			   BaseClass.logInfo("ROB Type is selected as", BaseClass.getfirstselectedoption(robType));
	           BaseClass.click(saveROB);
			   BaseClass.logInfo("Save Button is clicked", "");
	           Wait.waitFortextPresent(BaseClass.getDriver(), errorMessage);
	           Assert.assertEquals(BaseClass.gettext(errorMessage), "First Name is required.\nLast Name is required.");
	           BaseClass.logpass("Error Message should be displayed for Mandatory Fields", "Error Message is displayed for MandatoryFields as: First Name is required.\nLast Name is required.");
	           BaseClass.selectByVisibleText(robType, "Special Payee");
			   BaseClass.logInfo("ROB Type is selected as", BaseClass.getfirstselectedoption(robType));
	           BaseClass.click(saveROB);
			   BaseClass.logInfo("Save Button is clicked", "");
	           Wait.waitFortextPresent(BaseClass.getDriver(), errorMessage);
	           Assert.assertEquals(BaseClass.gettext(errorMessage), "Company Name is required.");
	           BaseClass.logpass("Error Message should be displayed for Mandatory Fields", "Error Message is displayed for MandatoryFields as: Company Name is required.");
		 
		   }
		   else
		   {
		   if(hm.get("ROBType").equals("Dependent"))
		   {
		   BaseClass.selectByVisibleText(robType, "Dependent");
		   BaseClass.logInfo("ROB Type is selected as", BaseClass.getfirstselectedoption(robType));
		   setDependentFields();
		    }
		   else
		   {
			   BaseClass.selectByVisibleText(robType, "Special Payee");
			   BaseClass.logInfo("ROB Type is selected as", BaseClass.getfirstselectedoption(robType));
			   BaseClass.click(attornyGrossProceed);
			   BaseClass.logInfo("Attorny Gross Proceed checkbox is clicked","");

		   }
		   setCommonFields(hm);
		   if(hm.get("Scenario").equals("Cancel"))
		   {
			   BaseClass.click(cancel);
			   BaseClass.logInfo("Cancel button is Clicked", "");
			   Wait.waitFor(2);
			   Assert.assertFalse(robType.isEnabled());
			   Assert.assertFalse(robCompanyName.isEnabled());
			   Assert.assertFalse(robEmail.isEnabled());
			   Assert.assertFalse(robFirstName.isEnabled());
			   Assert.assertFalse(robMiddleName.isEnabled());
			   Assert.assertFalse(robLastName.isEnabled());
			   Assert.assertFalse(robPhoneNumber.isEnabled());
			   Assert.assertFalse(isForeignAddressCheckBox.isEnabled());
			   Assert.assertFalse(address1.isEnabled());
			   Assert.assertFalse(address2.isEnabled());
			   Assert.assertFalse(city.isEnabled());
			   Assert.assertFalse(state.isEnabled());
			   Assert.assertFalse(postal.isEnabled());
			   Assert.assertFalse(attornyGrossProceed.isEnabled());
			   Assert.assertFalse(SSN.isEnabled());
			   Assert.assertFalse(DOB.isEnabled());
			   Assert.assertFalse(male.isEnabled());
			   Assert.assertFalse(female.isEnabled());
			   Assert.assertFalse(race.isEnabled());
			   Assert.assertFalse(guardian.isEnabled());
			   Assert.assertFalse(relationship.isEnabled());
			   Assert.assertFalse(total.isEnabled());
			   Assert.assertFalse(partial.isEnabled());
			   Assert.assertFalse(schoolingStatus.isEnabled());
			   BaseClass.logpass("All Fields should be disabled", "All Fields are disabled Successfully");
		   }
		   else
		   {
			   BaseClass.click(saveROB);
			   BaseClass.logInfo("Save Button is clicked", "");
	           Wait.waitFortextPresent(BaseClass.getDriver(), successMessage);
	           Assert.assertEquals(BaseClass.gettext(successMessage), "Saved successfully.");
			   BaseClass.logpass("ROB should be created", "ROB is created successfully");
		   }
		   }
	}
	@FindBy(xpath="//td[text()='Special Payee']/following-sibling::td/input")
	WebElement specialPayeeEditButton;
	
	@FindBy(xpath="//td[text()='Dependent']/following-sibling::td/input")
	WebElement dependentEditButton;
	
	public void validateROB(HashMap<String,String> hm)
	{
		if(hm.get("Scenario").equals("Update"))
		{
			createROB();
			if(hm.get("ROBType").equals("Dependent"))
			{
				BaseClass.click(dependentEditButton);
				setDependentFields();
			}
			else
			{
				BaseClass.click(specialPayeeEditButton);
			}
			setCommonFields(hm);
			 BaseClass.click(saveROB);
			   BaseClass.logInfo("Save Button is clicked", "");
	           Wait.waitFortextPresent(BaseClass.getDriver(), successMessage);
	           Assert.assertEquals(BaseClass.gettext(successMessage), "Saved successfully.");
			   BaseClass.logpass("ROB should be Updated", "ROB is Updated Successfully");
			
		}
		else
		{
			createROB(hm);
		}
	}
	
	private void setDependentFields()
	{
		BaseClass.settext(SSN, BaseClass.enterRandomNumber(9));
		   BaseClass.logInfo("SSN is set as", BaseClass.getattribute(SSN, "value"));
		   BaseClass.settext(DOB, BaseClass.pastdatefromTodayDate(BaseClass.generateRandomNumberAsInteger(20, 30), BaseClass.generateRandomNumberAsInteger(20, 30)));
		   BaseClass.logInfo("DOB is set as", BaseClass.getattribute(DOB, "value"));
		   BaseClass.clickRandomWebElement(male, female);
		   if(male.isSelected())
		   {
			   BaseClass.logInfo("Sex is selected as", "Male");
		   }
		   else
		   {
			   BaseClass.logInfo("Sex is selected as", "Female");
		   }
		   BaseClass.selectByIndex(race);
		   BaseClass.logInfo("Race is selected as", BaseClass.getfirstselectedoption(race));
		   BaseClass.settext(guardian, BaseClass.stringGeneratorl(5));
		   BaseClass.logInfo("Guardia is set as", BaseClass.getattribute(guardian, "value"));
		   BaseClass.selectByIndex(relationship);
		   BaseClass.logInfo("Relationship is selected as", BaseClass.getfirstselectedoption(relationship));
		   BaseClass.clickRandomWebElement(total, partial);
		   if(total.isSelected())
		   {
			   BaseClass.logInfo("Dependancy is selected as", "Total");
		   }
		   else
		   {
			   BaseClass.logInfo("Dependancy is selected as", "Partial");
		   }
		   BaseClass.selectByIndex(schoolingStatus);
		   BaseClass.logInfo("schoolingStatus is selected as", BaseClass.getfirstselectedoption(schoolingStatus));
		  
	}
	private void setCommonFields(HashMap<String,String> hm)
	{
		BaseClass.settext(robCompanyName, BaseClass.stringGeneratorl(5) +" "+BaseClass.stringGeneratorl(5));
		   BaseClass.logInfo("Company Name is set as", BaseClass.getattribute(robCompanyName, "value"));
		   BaseClass.settext(robEmail, "automation@pcisvision.com");
		   BaseClass.logInfo("Email is set as", BaseClass.getattribute(robEmail, "value"));
		   BaseClass.settext(robFirstName, BaseClass.stringGeneratorl(5));
		   BaseClass.logInfo("First Name is set as", BaseClass.getattribute(robFirstName, "value"));
		   BaseClass.settext(robMiddleName, BaseClass.stringGeneratorl(1));
		   BaseClass.logInfo("Middle Name is set as", BaseClass.getattribute(robMiddleName, "value"));
		   BaseClass.settext(robLastName, BaseClass.stringGeneratorl(5));
		   BaseClass.logInfo("Last Name is set as", BaseClass.getattribute(robLastName, "value"));
		   BaseClass.settext(robPhoneNumber, BaseClass.enterRandomNumber(10));
		   BaseClass.logInfo("Phone Number is set as", BaseClass.getattribute(robPhoneNumber, "value"));
		   if(hm.get("IsForeign").equalsIgnoreCase("Yes"))
		   {
			   BaseClass.click(isForeignAddressCheckBox);
			   BaseClass.logInfo("Is Foreign Address CheckBox is Checked","");
			   BaseClass.settext(foreignAddress1, BaseClass.stringGeneratorl(5));
			   BaseClass.logInfo("Foreign Address 1 is set as", BaseClass.getattribute(foreignAddress1, "value"));
			   BaseClass.settext(foreignAddress2, BaseClass.stringGeneratorl(5));
			   BaseClass.logInfo("Foreign Address 2 is set as", BaseClass.getattribute(foreignAddress2, "value"));
			   BaseClass.settext(foreignCity, BaseClass.stringGeneratorl(5));
			   BaseClass.logInfo("Foreign City is set as", BaseClass.getattribute(foreignCity, "value"));
			   BaseClass.settext(foreignCounty, BaseClass.stringGeneratorl(5));
			   BaseClass.logInfo("Foreign County is set as", BaseClass.getattribute(foreignCounty, "value"));
			   BaseClass.selectByIndex(foreignState);
			   BaseClass.logInfo("Foreign State is selected as", BaseClass.getfirstselectedoption(foreignState));
			   BaseClass.selectByIndex(foreignCountry);
			   BaseClass.logInfo("Foreign Country is selected as", BaseClass.getfirstselectedoption(foreignCountry));
			   BaseClass.settext(foreignZip, BaseClass.stringGeneratorl(5));
			   BaseClass.logInfo("Foreign Zip is set as", BaseClass.getattribute(foreignZip, "value"));
		   }
		   else
		   {
			   BaseClass.settext(address1, BaseClass.stringGeneratorl(5));
			   BaseClass.logInfo("Address 1 is set as", BaseClass.getattribute(address1, "value"));
			   BaseClass.settext(address2, BaseClass.stringGeneratorl(5));
			   BaseClass.logInfo("Address 2 is set as", BaseClass.getattribute(address2, "value"));
			   BaseClass.settext(postal, hm.get("Zip")+Keys.TAB);
			   BaseClass.logInfo("Postal is set as", BaseClass.getattribute(postal, "value"));
			   BaseClass.selectByIndex(city);
			   BaseClass.logInfo("City is selected as", BaseClass.getfirstselectedoption(city));
			   BaseClass.logInfo("State is selected as", BaseClass.getfirstselectedoption(state));
		   }
	}
}
