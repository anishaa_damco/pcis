package ClaimVisionPages;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.Select;
import org.testng.Assert;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class ClaimPage {
	
	private ClaimPage(){}
	private static final ClaimPage claimPg=new ClaimPage();

	public static ClaimPage getinstance()
	{
		return claimPg;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="Claim_claimDeniedRadioList_1")
	WebElement claimDeniedNo;
	
	@FindBy(id="Claim_claimDeniedRadioList_0")
	WebElement claimDeniedYes; 
	
	@FindBy(id="Claim_saveButton")
	WebElement claimSave; 
	
	@FindBy(id="Claim_saveMessageLabel")
	WebElement messageLabel; 
	
	@FindBy(css = "select#Claim_statusDropDown")
	WebElement status;
	
	@FindBy(css = "input#Claim_PolicyYearTextBox")
	WebElement policyYear;
	
	@FindBy(css = "input#Claim_stateClaimNumberTextBox")
	WebElement stateClaimNumber;
	
	@FindBy(css = "input#Claim_legacyClaimTextBox")
	WebElement legacyClaimNumber;
	
	@FindBy(css="input#Claim_saveButton")
	WebElement saveBtn;
	
	@FindBy(css="input#saveClaimImageButton")
	WebElement saveIcon;
	
	@FindBy(css = "input#openClaimImageButton")
	WebElement openClaimImageButton;
	
	@FindBy(css = "input#closeClaimImageButton")
	WebElement closeClaimIcon;
	//@FindBy(css ="div#claim_claimInfo_claimInfoPanel > table>tbody>tr")
	
	@FindBy(css = "div#Claim_claimInfo_claimInfoPanel > table > tbody > tr:nth-child(2) > td:nth-child(2)")
	WebElement claimStatusLabel;
	
	
	
	String claimPage,DiaryPage;
	
	
	@FindBy(css = "span#Claim_ClaimantTotal1_OSReservesLabel")
	WebElement osReserve;
	
	@FindBy(css = "input#Claim_deniedDateTextBox")
	WebElement deniedDateNonComp;
	
	
	@FindBy(css = "select#Claim_denialReasonDropDownList")
	WebElement deniedReasonNonComp;
	
	@FindBy(css = "input#Claim_IsIncludeAllPayCatInAggregateLimits")
	WebElement isIncludeAllPayCatCheckbox;
	
	@FindBy(css = "input#Claim_incidentDescriptionTextBox")
	WebElement incidentDescription;
	
	@FindBy(css="span#Claim_incidentDescDisplayCharCount_countLabel > span")
	public WebElement incidentDescCountLabel;
	
	@FindBy(css = "textarea#Claim_summaryTextBox")
	public WebElement claimSummary;
	
	@FindBy(css="span#Claim_summaryDisplayCharCount_countLabel > span")
	public WebElement summaryDisplayCountLabel;
	
	@FindBy(css = "textarea#Claim_damagesTextBox")
	public WebElement claimDamages;
	
	@FindBy(css="span#Claim_damagesDisplayCharCount_countLabel > span")
	public WebElement damagesCountLabel;
	
	@FindBy(css="input#openClaimImageButton")
	WebElement openClaimIcon;
	
	@FindBy(css = "select#Claim_statusDropDown")
	WebElement statusDropDown;
	
	@FindBy(id="btnClose")
	WebElement closedBtnForOpenDiaries;
	
	@FindBy(css = "input#Claim_cancelButton")
	WebElement cancel;
	
	public void updateClaimWithPermission(String Scenario)
	{
		if (Scenario.equals("Denied"))
		{
			BaseClass.click(claimDeniedYes);
		}
		else
		{
			BaseClass.click(claimDeniedNo);
		}
		BaseClass.click(claimSave);
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved Successfully.");
		
	} 
	
	
	public void includeAllPayCatInAggregateLimits()
	{
		if(!isIncludeAllPayCatCheckbox.isSelected())
		{
		BaseClass.click(isIncludeAllPayCatCheckbox);
		Assert.assertTrue(isIncludeAllPayCatCheckbox.isSelected());
		}
		else{
			BaseClass.click(isIncludeAllPayCatCheckbox);
			Assert.assertFalse(isIncludeAllPayCatCheckbox.isSelected());
		}
		BaseClass.logInfo(" Clicked on Include All Payment Categories In Aggregate Limits check box", "" );
		BaseClass.click(claimSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
	}
	
	public void claimDenied(){
		if(claimDeniedYes.isSelected())
		{
			BaseClass.click(claimDeniedNo);
			BaseClass.logInfo("Claim denied radio button 'No' is selected", "");
			Assert.assertEquals(true, claimDeniedNo.isSelected());
			
		}
		else
		{
			BaseClass.click(claimDeniedYes);
			Wait.waitTillPresent(BaseClass.getDriver(), deniedDateNonComp);
			Assert.assertTrue(deniedReasonNonComp.isDisplayed());
			BaseClass.settext(deniedDateNonComp, BaseClass.getCurrentDate());
			BaseClass.logInfo("Denied date entered as ", BaseClass.gettext(deniedDateNonComp));
			BaseClass.selectByIndex(deniedReasonNonComp, 2);
			BaseClass.logInfo("Denied reason is selected as ", BaseClass.getfirstselectedoption(deniedReasonNonComp));
			
		}
		BaseClass.click(claimSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
	}
	
	public void openStatusOfClaim(HashMap<String, String> hm){
		if(hm.get("StatusChange").contains("Icon")){
			BaseClass.click(openClaimIcon);
			BaseClass.logInfo("Clicked on Open claim icon", "");
			BaseClass.acceptalert();
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			BaseClass.logInfo("Alert accepted for opening the claim", "");
			
		}
		else if (hm.get("StatusChange").contains("Drop-down")){
			BaseClass.selectByVisibleText(statusDropDown, "Open");
			BaseClass.logInfo("Claim status as Open selected from drop-down", "");
			BaseClass.click(claimSave);
			BaseClass.logInfo("Save Button is Clicked", "");
			Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
			BaseClass.logInfo("Success Message should be displayed", "Success Message is displayed");
			}
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains( "Open"));
		BaseClass.logpass("Successfully opened the claim", "Successfully opened the claim");
	
	}
	
	public HashMap<String, String>  getTotalAmountListClaimScreen(){
		HashMap<String, String> hm= new HashMap<String, String>();;
		List<HashMap> list = new ArrayList<HashMap>();
		//*[@id="Claim_closingclaimdiv"]/table[2]/tbody/tr[10]/td/div/fieldset/table/tbody/tr[2]
		//int lastRowcount=BaseClass.getDriver().findElements(By.xpath("//*[@id='FinSum_FinancialSummaryGridView']/tbody/tr")).size();
		//reading row which is having total amount of each column
		List<WebElement> allColumnOfClaimFinancials =BaseClass.getDriver().findElements(By.xpath("//*[@id='Claim_closingclaimdiv']/table[2]/tbody/tr[10]/td/div/fieldset/table/tbody/tr[2]/td"));
		List<WebElement> allColumnHeading =BaseClass.getDriver().findElements(By.xpath("//*[@id='Claim_closingclaimdiv']/table[2]/tbody/tr[10]/td/div/fieldset/table/tbody/tr[1]/th"));
	     int colsize = allColumnOfClaimFinancials.size();
	     int colheadsize = allColumnHeading.size();
	     for(;colsize>1;colsize--,colheadsize--)
	     {
	    	 hm.put(allColumnHeading.get(colheadsize-1).getText(), allColumnOfClaimFinancials.get(colsize-1).getText()) ;
	     }
	   
	    	
	    	
	   return hm;
		
	}
	
	public void reClosedStatusOfClaim(HashMap<String, String> hm){
		/*DashboardPage.getinstance().navigateToFinancialSummary();
		Wait.waitForTitle(BaseClass.getDriver(), "Financial Summary");
		BaseClass.logInfo("Navigate to Financial Summary Screen", "");
		// or can be navigate to financial summary screen
		// if pending have value - then pending error message will display
		// if paid & reserves have value then outstanding reserves error message will display
		*/
		HashMap<String, String> financialTotal = getTotalAmountListClaimScreen();
		//DashboardPage.getinstance().navigateToClaim();
		//Wait.waitForTitle(BaseClass.getDriver(), "Claim");
		//BaseClass.logInfo("Navigate to Claim Screen again", "");
		claimPage=BaseClass.getcurrentwindow();
		//BaseClass.selectByIndex(claimClosedReason, BaseClass.generateRandomNumberAsInteger(4));
		//BaseClass.logInfo("Closed reason selected as: ", BaseClass.getfirstselectedoption(claimClosedReason));
		System.out.println("hm: "+Arrays.asList(hm));
		if(hm.get("StatusChange").contains("Icon")){
			
			BaseClass.click(closeClaimIcon);
			BaseClass.logInfo("Clicked on close claim icon", "");
			
			
		}
		else if (hm.get("StatusChange").contains("Drop-down")){
			BaseClass.selectByIndex(statusDropDown,2);
			BaseClass.logInfo("Claim status is selected from drop-down as: ", BaseClass.getfirstselectedoption(statusDropDown));
			BaseClass.click(claimSave);
			BaseClass.logInfo("Save Button is Clicked", "");
							
		}
		BaseClass.acceptalert();
		Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
		BaseClass.logInfo("Alert for closing the claim is accepted", "");
		
		
		if((!(financialTotal.get("Paid").contains("$0.00"))
				|| (!financialTotal.get("OS Reserves").contains("$0.00"))))
		{
			//outstanding reserves error message will display
			//need to put assert condition
			BaseClass.logInfo("Getting outstanding reserve error message while closing the claim", "");
			BaseClass.logInfo("Claim having OS Reserve amount: ", financialTotal.get("OS Reserves"));
			BaseClass.logInfo("Claim having Paid amount: ", financialTotal.get("Paid"));
		}
		
		else if ((!financialTotal.get("Pending").contains("$0.00"))
				|| hm.get("PaymentStatus").equals("7")
				||((!financialTotal.get("Paid").contains("$0.00"))
						&&(financialTotal.get("Pending").contains("$0.00")) 
								&& (financialTotal.get("OS Reserves").contains("$0.00")))){
			//pending error message will display
			//need to put assert condition
			System.out.println("Entered in Pending condition verification");
			BaseClass.logInfo("Getting pending payment error message while closing the claim", "");
			BaseClass.logInfo("Claim having Pending amount: ", financialTotal.get("Pending"));
		}
		else {
			//open diaries pop-up handling
			if(BaseClass.getDriver().getWindowHandles().size()>1)
			{
				DiaryPage=BaseClass.switchToNextWindow(claimPage,"Open Diaries", 2);
				BaseClass.logInfo("Diary Page is displayed", "");
				BaseClass.click(closedBtnForOpenDiaries);
				BaseClass.logInfo("Diaries pop-up is closed successfully", "");
				BaseClass.switchToWindow(claimPage,1);
			}
			
			Assert.assertEquals(BaseClass.gettext(messageLabel), "The claim was closed successfully.");
			Wait.waitFortextPresent(BaseClass.getDriver(), claimStatusLabel);
			System.out.println(BaseClass.gettext(claimStatusLabel));
			Assert.assertTrue(BaseClass.gettext(claimStatusLabel).contains("Closed")) ;
			BaseClass.logpass("Successfully closed the claim", "Successfully closed the claim");
		}
	
	}
	
	//public void closedStatusOfClaim(HashMap<String, String> hm){}
	
	
	public void claimStatusVerification(HashMap<String, String> hm)
	{
		if(hm.get("Scenario").contains("ReOpened")){
			if(BaseClass.gettext(claimStatusLabel).contains("Closed") ||
					BaseClass.gettext(claimStatusLabel).contains("ReClosed"))
				//reOpenedStatusOfClaim(hm);
				openStatusOfClaim(hm);
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-opened status", "");
				}
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-opened status", "");
				}
			}
				
		}
		else if(hm.get("Scenario").contains("ReClosed") ){
			if(BaseClass.gettext(claimStatusLabel).contains("ReOpened")){
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Closed")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-closed status", "");
				}
				reClosedStatusOfClaim(hm);
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				if(BaseClass.gettext(claimStatusLabel).contains("Closed"))
					openStatusOfClaim(hm);
				else{
					BaseClass.logInfo("No closed claim is present to verify re-closed status", "");
				}
				reClosedStatusOfClaim(hm);
			}
				
		}
		
		else if(hm.get("Scenario").contains("Closed")){
			if(BaseClass.gettext(claimStatusLabel).contains("Open")){
				reClosedStatusOfClaim(hm);
				
			}
			else if(BaseClass.gettext(claimStatusLabel).contains("Pending")){
				openStatusOfClaim(hm);
				reClosedStatusOfClaim(hm);
				
			}
			
		}
		else if(hm.get("Scenario").contains("Open")){
			openStatusOfClaim(hm);
			}
	}
	
	
	/*public void claimScreen(HashMap<String, String> hm)
	{
		System.out.println(BaseClass.gettext(claimStatus) + "before changing the status");
		if(hm.get("Scenario").equals("Open")){
			BaseClass.selectByVisibleText(status, hm.get("Scenario"));
			System.out.println(BaseClass.getfirstselectedoption(status));
		}
		else if (hm.get("Scenario").equals("Closed")){
			BaseClass.selectByVisibleText(status, hm.get("Scenario"));
			System.out.println(BaseClass.getfirstselectedoption(status));
			}
		else if (hm.get("Scenario").equals("ReClosed"))
			BaseClass.click(closeClaimImageButton);
		else if	(hm.get("Scenario").equals("ReOpened")) 
			BaseClass.click(openClaimImageButton);
		
		//BaseClass.acceptalert();
		
		if(hm.get("Save").equals("Button"))
			BaseClass.click(saveBtn);
		else if(hm.get("Save").equals("Icon"))
			BaseClass.click(saveIcon); 
		System.out.println(BaseClass.gettext(osReserve));
		System.out.println(BaseClass.gettext(claimStatus) + "after status change");
		//accepting outstanding reserve pop-up if any reserves exists
		if(!BaseClass.gettext(osReserve).equals("$0.00") &&
				(hm.get("Scenario").equals("ReClosed") ||
						hm.get("Scenario").equals("Closed")))
			BaseClass.acceptalert();
       
        	
		System.out.println(hm.get("Scenario"));
		BaseClass.logInfo("Status is selected a"
				+ "s", BaseClass.getfirstselectedoption(status));
		Assert.assertTrue(BaseClass.gettext(claimStatus).equals(hm.get("Scenario")));
		//Assert.assertTrue(BaseClass.getfirstselectedoption(status).equals(hm.get("Scenario")));
	}
	*/
	public void limitVerification(String Scenario) 
	{
		if(Scenario.equals("Incident Description")){
			BaseClass.settext(incidentDescription, BaseClass.stringGeneratorl(75));
			BaseClass.logInfo("Entered incident desciption as : ", BaseClass.getattribute(incidentDescription, "value"));
			String countLabelValue =BaseClass.gettext(incidentDescCountLabel);
			BaseClass.logInfo("Incident description length is: ", countLabelValue);
			Assert.assertTrue(countLabelValue.contains("75/75"));
			BaseClass.logInfo("Not able to enter incident description more than it's length", "");
		}
		else if (Scenario.equals("Claim Summary"))
		{
			BaseClass.settext(claimSummary, BaseClass.stringGeneratorl(250));
			BaseClass.logInfo("Entered claim summary as : ", BaseClass.getattribute(claimSummary, "value"));
			String countLabelValue =BaseClass.gettext(summaryDisplayCountLabel);
			BaseClass.logInfo("Claim summary length is : ", countLabelValue);
			Assert.assertTrue(countLabelValue.contains("250/250"));
			BaseClass.logInfo("Not able to enter Claim summary more than it's length", "");
		}
		else if (Scenario.equals("Claim Damages"))
		{
			BaseClass.settext(claimDamages, BaseClass.stringGeneratorl(250));
			BaseClass.logInfo("Entered claim damages as : ", BaseClass.getattribute(claimDamages, "value"));
			String countLabelValue =BaseClass.gettext(damagesCountLabel);
			BaseClass.logInfo("Claim damages length is : ", countLabelValue);
			Assert.assertTrue(countLabelValue.contains("250/250"));
			BaseClass.logInfo("Not able to enter Claim damages more than it's length", "");
		}
		BaseClass.click(claimSave);
		BaseClass.logInfo("Save Button is Clicked Successfully", "");
		Assert.assertEquals(BaseClass.gettext(messageLabel), "Saved successfully.");
		BaseClass.logpass("Success Message should be displayed", "Success Message is displayed");
	}
	
	public void cancelButtonVerify() {
		String incidentDes = null;
		incidentDes =BaseClass.getattribute(incidentDescription, "value"); 
		BaseClass.logInfo("Incident description before entering new data: ", incidentDes);
		BaseClass.settext(incidentDescription, BaseClass.stringGeneratorl(5));
		BaseClass.logInfo("Entered the incident desciption : ", BaseClass.getattribute(incidentDescription, "value"));
		//BaseClass.logInfo("Incident description length after entering data: ", BaseClass.getattribute(incidentDescCountLabel, "value"));
		BaseClass.click(cancel);
		BaseClass.logInfo("Clicked on cancel button", "");
		Wait.waitforelement(BaseClass.getDriver(), incidentDescription);
		Wait.waitForContainValue(incidentDescription, incidentDes);
		BaseClass.logInfo("Incident description after clicked on cancel button: ", BaseClass.getattribute(incidentDescription, "value"));
		Assert.assertEquals(incidentDes, BaseClass.getattribute(incidentDescription, "value"));
		BaseClass.logpass("Verified cancel button is working as expected","");
		
	}
	
	
	
	
	
}
