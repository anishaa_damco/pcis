package ClaimVisionPages;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;

import CommonModules.BaseClass;

public class WageWorkRTWPage {
	
	private WageWorkRTWPage(){}
	private static final WageWorkRTWPage wageWorkRTWPage=new WageWorkRTWPage();

	public static WageWorkRTWPage getinstance()
	{
		return wageWorkRTWPage;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="claimsTreeViewt6")
	WebElement wageWorkLink;
	
	@FindBy(id="RTW_wageRateTextBox")
	WebElement wageRate; 
	
	@FindBy(id="RTW_perDropDown")
	WebElement per; 
	
	@FindBy(id="RTW_daysWorkedWeekDropDown")
	WebElement days; 
	
	@FindBy(id="RTW_hrsDaysTextBox")
	WebElement hrs; 
	
	@FindBy(id="RTW_DisabilitySeverityDropDown")
	WebElement disabilitySeverity; 
	
	@FindBy(id="RTW_saveButton")
	WebElement save; 
	
	@FindBy(id="RTW_saveMessageLabel")
	WebElement messagelabel; 
	
	
	public void updatewagerate()
	{
		//BaseClass.Scrolltoelement(wageWorkLink);
		BaseClass.click(wageWorkLink);
		BaseClass.settext(wageRate, BaseClass.enterRandomNumber(2));
		BaseClass.selectByVisibleText(per, "Weekly");
		BaseClass.selectByVisibleText(days, "5");
		BaseClass.settext(hrs, "2");
		BaseClass.selectByVisibleText(disabilitySeverity, "Max");
		BaseClass.click(save);
		Assert.assertEquals(BaseClass.gettext(messagelabel), "Saved successfully.");
	} 
}
