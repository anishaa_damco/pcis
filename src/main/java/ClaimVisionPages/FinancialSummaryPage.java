package ClaimVisionPages;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import CommonModules.BaseClass;
import CommonModules.Wait;

public class FinancialSummaryPage {

	private FinancialSummaryPage(){}
	private static final FinancialSummaryPage FinancialSummary=new FinancialSummaryPage();
	List<String> wcFinancialSummaryList = new ArrayList<String>();
	public static FinancialSummaryPage getinstance()
	{
		return FinancialSummary;
	}
	public void initElement(WebDriver driver)
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="#headerControl_itemName")
	WebElement FinancialSummaryHeading;
	
	public HashMap<String, String>  getTotalAmountList(){
		HashMap<String, String> hm= new HashMap<String, String>();;
		List<HashMap> list = new ArrayList<HashMap>();
		int lastRowcount=BaseClass.getDriver().findElements(By.xpath("//*[@id='FinSum_FinancialSummaryGridView']/tbody/tr")).size();
		//reading row which is having total amount of each column
		List<WebElement> allColumnOfLastRow =BaseClass.getDriver().findElements(By.xpath("//*[@id='FinSum_FinancialSummaryGridView']/tbody/tr["+lastRowcount+"]/td"));
		List<WebElement> allColumnHeading =BaseClass.getDriver().findElements(By.xpath("//*[@id='FinSum_FinancialSummaryGridView']/tbody/tr[1]/th"));
	     int colsize = allColumnOfLastRow.size();
	     int colheadsize = allColumnHeading.size();
	     for(;colsize>1;colsize--,colheadsize--)
	     {
	    	 hm.put(allColumnHeading.get(colheadsize-1).getText(), allColumnOfLastRow.get(colsize-1).getText()) ;
	     }
	   
	    	
	    	
	   return hm;
		
	}

	
	
	public void expandAll()
	{
		List<WebElement> expandbuttons = BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'hiddenImageButton')]"));
		for(int i=0;i<expandbuttons.size();i++)
		{
			Wait.waitFor(2);
			if(BaseClass.getattribute(BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'hiddenImageButton')]")).get(i), "src").contains("Collapse"))
			{
				BaseClass.click(BaseClass.getDriver().findElements(By.xpath("//input[contains(@id,'hiddenImageButton')]")).get(i));					}
		}
	}


}
